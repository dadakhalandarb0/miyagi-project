import {
  Routes,
  Route,
  useNavigationType,
  useLocation,
} from "react-router-dom";
import FrameComponent from "./pages/HomePages/FrameComponent";
import { useEffect } from "react";
import VerifyNow from "./pages/Verify";
import UserAccount from "./pages/UserAccount";
import Dashboard from "./pages/AllUserPages/Dashboard";
import MyData from "./pages/AllUserPages/MyData";
import SyncFile from "./pages/AllUserPages/SyncFile";
import Trash from "./pages/AllUserPages/Trash";
import Settings from "./pages/AllUserPages/Settings";
import Philosophy from "./pages/Philosophy";
import LoginPage from "./pages/LoginPage";
import SignUp from "./pages/SignUp";
import LoginOTP from "./pages/LoginOTP";
import ProfileDetail from "./pages/ProfileDetail";
import Subscription from "./pages/Subscription";
import ProjectsPage from "./pages/ProjectsPage";
import Services from "./pages/Services";
import CreateUserForm from "./pages/Services/CreateUserForm";
import WaitList from "./pages/WaitList";
import RegisterOrganization from "./pages/RegisterOrganization";
import RegisterAffiliation from "./pages/RegisterAffiliation";
import Affiliation from "./pages/Affiliation";

function App() {
  const action = useNavigationType();
  const location = useLocation();
  const pathname = location.pathname;

  useEffect(() => {
    if (action !== "POP") {
      window.scrollTo(0, 0);
    }
  }, [action, pathname]);

  useEffect(() => {
    let title = "";
    let metaDescription = "";

    switch (pathname) {
      case "/":
        title = "";
        metaDescription = "";
        break;
    }

    if (title) {
      document.title = title;
    }

    if (metaDescription) {
      const metaDescriptionTag = document.querySelector(
        'head > meta[name="description"]'
      );
      if (metaDescriptionTag) {
        metaDescriptionTag.content = metaDescription;
      }
    }
  }, [pathname]);

  return (
    <Routes>
      <Route path="/" element={<FrameComponent />} />
      <Route path='/varifynow' element={<VerifyNow />} />
      <Route path='/philosophy' element={<Philosophy />} />
      <Route path="/services" element={<Services />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path='/login/otp' element={<LoginOTP />} />
      <Route path="/signup" element={<SignUp />} />
      <Route path="/waitlist/profiledetail" element={<ProfileDetail />} />
      <Route path="/waitlist/profiledetail/subscription" element={<Subscription />} />
      <Route path="/waitlist/profiledetail/subscription/registerOrganization" element={<RegisterOrganization />} />
      <Route path='/waitlist' element={<WaitList />} />
      <Route path="/verifynow/registerAffiliation" element={<RegisterAffiliation />} />
      <Route path="/verifynow/affiliation" element={<Affiliation />} />

      <Route path="/createuserform" element={<CreateUserForm />} />

      <Route path="/user/*" element={
        <UserAccount>
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/mydata" element={<MyData />} />
            <Route path="/sync" element={<SyncFile />} />
            <Route path="/trash" element={<Trash />} />
            <Route path="/settings" element={<Settings />} />
          </Routes>
        </UserAccount>} />
    </Routes>
  );
}
export default App;
