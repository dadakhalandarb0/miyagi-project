const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
const cors = require("cors"); // Import the cors module

const app = express();
const port = 4000;

// Middleware
app.use(bodyParser.json());
app.use(cors()) // Use the cors middleware to allow cross-origin requests

// Multer setup
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

// Example route
app.get("/", (req, res) => {
    res.send("Backend is up and running!");
});

// Example POST route with file upload
app.post("/createUser", upload.single("profilePicture"), (req, res) => {
    const { userName } = req.body;
    const profilePictureData = req.file;

    if (!userName || !profilePictureData) {
        return res.status(400).send("Missing required fields");
    }

    // Perform user creation logic here

    const response = {
        success: true,
        message: "User created successfully",
    };

    res.json(response);
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
