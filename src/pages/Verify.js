import Header from "./HeaderComponent";
import VerifyOne from "./VerifyNow/VerifyOne";

const VerifyNow = () => {
    return (
        <div>
        <Header />
        <VerifyOne />
        </div>
    )
}

export default VerifyNow;