import { useState } from "react";
import './HeaderComponent.css';
import { Link } from 'react-router-dom';
import PhilosophyFrameOne from "./PhilosophyFrames/PhilosophyFrameOne";
import PhilosophyFrameTwo from "./PhilosophyFrames/PhilosophyFrameTwo";
import PhilosophyFrameThree from "./PhilosophyFrames/PhilosophyFrameThree";
import PhilosophyFrameFour from "./PhilosophyFrames/PhilosophyFrameFour";
import './Philosophy.css';

const Philosophy = () => {
    const [color, setColor] = useState(false);
    const changeColor = () => {
        if (window.scrollY >= 100) {
            setColor(true);
        } else {
            setColor(false);
        }
    }
    window.addEventListener('scroll', changeColor);

    return (
        <div className="mainContainerPhilosophy">
            <div className={color ? 'header header-bg' : 'header'}>
                <Link to='/'>
                    <div className={color ? "logo fs-2 text-dark fw-bold" : "logo fs-2 text-dark fw-bold"}><span style={{ color: '#0069ec' }}>&lt;</span>MIYAGI<span style={{ color: '#0069ec' }}>&gt;</span></div>
                </Link>
                <nav>
                    <ul className={color ? "ulScroll mb-2 mt-2 p-0" : "mb-2 mt-2 p-0"}>
                        <li style={{ position: 'relative', top: '-4px' }}>
                            <a href="/" className="style-2 ">HOME</a>
                        </li>
                        <li style={{ position: 'relative', top: '-3px' }}>
                            <a href="/philosophy" className="style-2 ">PHILOSOPHY</a>
                        </li>
                        <li>
                        </li>
                        <li style={{ position: 'relative', top: '-3px' }}>
                            <a href="/services" className="style-2 ">SERVICES</a>
                        </li>
                        <li style={{ position: 'relative', top: '-4px' }}>
                            <a href="#" className="style-2 ">SUBSCRIPTION</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <PhilosophyFrameOne />
            <PhilosophyFrameTwo />
            <PhilosophyFrameThree />
            <PhilosophyFrameFour />
            <div className="philosophyFooter">
                <h6><span>&lt;</span>Miyagi<span>&gt;</span></h6>
                <p>
                    A <span>decentralised</span> application built for the curious,<br />
                    By the a bunch of <span>dreamers</span> & <span>tinkers</span> in the <span>Indian Subcontinent</span>.
                </p>
            </div>
        </div>
    )
}

export default Philosophy;