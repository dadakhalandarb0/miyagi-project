import { Link } from 'react-router-dom';
import './WaitList.css';
import { useState } from 'react';

const WaitList = () => {
    const [showLoginFields, setShowLoginFields] = useState(false);
    console.log(showLoginFields, 'check');
    const handleEarlyAccessClick = () => {
        setShowLoginFields(true);
    };

    return (
        <div className="WaitListMainContainer">
            <img className='cube000' src="1_3cube.svg" />
            <img className='cube111' src="1_3cube.svg" />
            <img className='cube222' src="1_3cube.svg" />
            <img className='circle222' src='Ellipse 13blue.svg' />
            <div className='homebuttonlogo'>
                <Link to='/'>
                    &lt;<span style={{ color: 'black' }}>HOME</span>&gt;
                </Link>
            </div>
            <div className='partOne'>
                <h2>&lt;WaitList&gt;</h2>
                <h6>&lt;
                    <span>Thank you</span> for showing your interst, please<br />
                    please signup for the <span>waitlist</span> or fill the<br />
                    the account details for the pre-launch<br />
                    account if you are assinged one&gt;</h6>
                <div className="row">
                    <span>
                        <input className="basic-slide shadow" id="email" type="text" placeholder="Your favorite email" />
                        <label htmlFor="email">Email</label>
                    </span>
                </div>
                <Link to='/login/otp'>
                    <button className='continueButton'>
                        Continue
                    </button>
                </Link>
            </div>
            <div className='partTwo'>
                <a className="btnfos" onClick={handleEarlyAccessClick}>
                    I've got the early access!
                </a>
                {showLoginFields && (
                    <>
                        <div className="login-fields">
                            <input type="text" placeholder="Email" />
                            <input type="password" placeholder="Password" />
                        </div>
                        <Link to='/waitlist/profiledetail'>
                            <button className='waitListSubmit'>submit</button>
                        </Link>
                    </>
                )}
            </div>
        </div>
    )
}

export default WaitList;