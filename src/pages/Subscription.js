import { Link } from 'react-router-dom';
import React, { useState } from 'react';
import './Subscription.css';

const Subscription = () => {

    const [showMonthlyPrice, setShowMonthlyPrice] = useState(true);

    const handlePriceChange = () => {
        setShowMonthlyPrice(!showMonthlyPrice);
    };

    return (
        <div className="SubscriptionMainContainer">
            <img className='cube000' src="../../1_3cube.svg" />
            <img className='cube111' src="../../1_3cube.svg" />
            <img className='cube222' src="../../1_3cube.svg" />
            <img className='circle222' src='../../Ellipse 13blue.svg' />
            <div className='homebuttonlogo'>
                <Link to='/'>
                    &lt;<span style={{ color: 'black' }}>Miyagi</span>&gt;
                </Link>
            </div>
            <div className='partOne'>
                <div className='subscriptionPackBox'>
                    <h1>Our Pricing</h1>
                    <div className="toggle-selector">
                        <h2>Annually</h2>
                        <div id="toggle-space" onClick={handlePriceChange}>
                            <div className={`${showMonthlyPrice ? 'toggle-ball' : 'active'}`}></div>
                        </div>
                        <h2>Monthly</h2>
                    </div>
                    <div className="cards">
                        {/* Card One */}
                        <div className="card-one">
                            <h2>Basic</h2>
                            <span className={showMonthlyPrice ? 'price monthly' : 'price annually'}>
                                <span className="dollar-span">Rs</span>
                                {showMonthlyPrice ? 1399 : 13999}
                            </span>
                            <hr />
                            <p>500 GB Storage</p>
                            <hr />
                            <p>2 Users Allowed</p>
                            <hr />
                            <p>Send up to 3 GB</p>
                            <hr />
                            <button>Learn More</button>
                        </div>

                        {/* Card Two */}
                        <div className="card-two">
                            <h2 className="header-two">Professional</h2>
                            <span className={showMonthlyPrice ? 'price monthly' : 'price annually'}>
                                <span className="dollar-span">Rs</span>
                                {showMonthlyPrice ? 1799 : 17999}
                            </span>
                            <hr />
                            <p>1 TB Storage</p>
                            <hr />
                            <p>5 Users Allowed</p>
                            <hr />
                            <p>Send up to 10 GB</p>
                            <hr />
                            <button>Learn More</button>
                        </div>

                        {/* Card Three */}
                        <div className="card-three">
                            <h2>Master</h2>
                            <span className={showMonthlyPrice ? 'price monthly' : 'price annually'}>
                                <span className="dollar-span">Rs</span>
                                {showMonthlyPrice ? 2799 : 27999}
                            </span>
                            <hr />
                            <p>2 TB Storage</p>
                            <hr />
                            <p>10 Users Allowed</p>
                            <hr />
                            <p>Send up to 20 GB</p>
                            <hr />
                            <button>Learn More</button>
                        </div>
                    </div>
                </div>
                <div className='subscriptionTextBox'>
                    <p>&lt;That was a long signup process but we are glad to tell you that it is over,
                        since you have been very patient we would like to give you a token of
                        gratitude in form of our assets to try out the application, if you want to opt
                        for the subsubscription anyways we would add the 2 assets to your account
                        after you choose a plan&gt;
                    </p>
                </div>
            </div>
            <div className='partTwo'>
                <div className="subBtnfos" typeof='button' data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                    Go for the subscription
                </div>
                <Link to='/waitlist/profiledetail/subscription/registerOrganization'>
                    <div className="subBtnfos">
                        Try out the Free Trial
                    </div>
                </Link>
            </div>
            <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg">
                    <div className="modal-content">
                        <div className="modal-body d-flex flex-column">
                            <button type="button" className="btn-close align-self-end" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div className='subscriptionPackBox'>
                                <h1>Our Pricing</h1>
                                <div className="toggle-selector">
                                    <h2>Annually</h2>
                                    <div id="toggle-space" onClick={handlePriceChange}>
                                        <div className={`${showMonthlyPrice ? 'toggle-ball' : 'active'}`}></div>
                                    </div>
                                    <h2>Monthly</h2>
                                </div>
                                <div className="cards">
                                    {/* Card One */}
                                    <div className="card-one">
                                        <h2>Basic</h2>
                                        <span className={showMonthlyPrice ? 'price monthly' : 'price annually'}>
                                            <span className="dollar-span">Rs</span>
                                            {showMonthlyPrice ? 1399 : 13999}
                                        </span>
                                        <hr />
                                        <p className='text-light fw-bold'>500 GB Storage</p>
                                        <hr />
                                        <p className='text-light fw-bold'>2 Users Allowed</p>
                                        <hr />
                                        <p className='text-light fw-bold'>Send up to 3 GB</p>
                                        <hr />
                                        <button>Select</button>
                                    </div>

                                    {/* Card Two */}
                                    <div className="card-two">
                                        <h2 className="header-two">Professional</h2>
                                        <span className={showMonthlyPrice ? 'price monthly' : 'price annually'}>
                                            <span className="dollar-span">Rs</span>
                                            {showMonthlyPrice ? 1799 : 17999}
                                        </span>
                                        <hr />
                                        <p className='fw-bold'>1 TB Storage</p>
                                        <hr />
                                        <p className='fw-bold'>5 Users Allowed</p>
                                        <hr />
                                        <p className='fw-bold'>Send up to 10 GB</p>
                                        <hr />
                                        <button>Select</button>
                                    </div>

                                    {/* Card Three */}
                                    <div className="card-three">
                                        <h2>Master</h2>
                                        <span className={showMonthlyPrice ? 'price monthly' : 'price annually'}>
                                            <span className="dollar-span">Rs</span>
                                            {showMonthlyPrice ? 2799 : 27999}
                                        </span>
                                        <hr />
                                        <p className='text-light fw-bold'>2 TB Storage</p>
                                        <hr />
                                        <p className='text-light fw-bold'>10 Users Allowed</p>
                                        <hr />
                                        <p className='text-light fw-bold'>Send up to 20 GB</p>
                                        <hr />
                                        <button>Select</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer border-0">
                            <a href="#" className="btnCancel btn-white btn-animate" data-bs-dismiss="modal">Cancel</a>
                            <a href="/signup/profiledetail/subscription/projects" className="btnConfirm btn-white btn-animate">Confirm</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Subscription;