import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './RegisterOrganization.css';

class RegisterOrganization extends Component {
    state = {
        imageUrl: 'https://images.unsplash.com/photo-1575936123452-b67c3203c357?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aW1hZ2V8ZW58MHx8MHx8fDA%3D&w=1000&q=80', // Replace with the actual URL of your image
    };

    render() {
        const { imageUrl } = this.state;

        const divStyle = {
            backgroundImage: `url(${imageUrl})`,
            backgroundSize: 'cover', // Adjust as needed
            backgroundPosition: 'center', // Adjust as needed
            width: '200px', // Adjust the width of the div
            height: '200px', // Adjust the height of the div
            border: '1px solid #ccc', // Optional: Add a border for visual reference
        };

        return (
            <div className="RegisterOrganizationMainContainer">
                <img className='circle2222' src='/Ellipse 13blue.svg' />
                <div className='homebuttonlogo'>
                    <Link to='/'>
                        &lt;<span style={{ color: 'black' }}>Miyagi</span>&gt;
                    </Link>
                </div>
                <div className='partOne'>
                    <h6>&lt;Register your organisation on <span>Miyagi,</span>&gt;</h6>
                    <div className='previewProfilePic' style={divStyle}>
                    </div>
                    <h6>Name of the Organisation</h6>
                    <div className="Organisation-fields">
                        <input type="text" placeholder="Website Link" />
                        <input type="tel" id="phone" name="phone" maxLength="10" placeholder="123-45-678" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" required />
                    </div>
                </div>
                <div className='partTwo'>
                    <div className="Organisation-fields">
                        <input type="text" placeholder="Your Designation at the Organisation" />
                        <Link to='/user/dashboard'>
                            <button className='OrganisationSubmit'>Begin the Verification</button>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default RegisterOrganization;
