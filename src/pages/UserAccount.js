import { Link } from 'react-router-dom';
import './UserAccount.css';
import React, { useEffect, useRef, useState } from 'react';

const UserAccount = ({ children }) => {
    const menuItem = [
        {
            path: "/user/dashboard",
            name: "Dashboard",
            icon: "fa-solid fa-table-columns fs-4"
        },
        {
            path: "/user/mydata",
            name: "MyData",
            icon: "fa-solid fa-folder fs-4"
        },
        {
            path: "/user/sync",
            name: "Sync",
            icon: "fa-solid fa-rotate fs-4"
        },
        {
            path: "/user/trash",
            name: "Trash",
            icon: "fa-solid fa-trash fs-4"
        },
        {
            path: "/user/settings",
            name: "Settings",
            icon: "fa-solid fa-gear fs-4"
        },
    ]

    const [activeIndex, setActiveIndex] = useState(-1);
    const menuRef = useRef(null); // Ref to the menu container

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (!menuRef.current.contains(event.target)) {
                setActiveIndex(-1);
            }
        };

        document.addEventListener('mousedown', handleClickOutside);

        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    const handleButtonClick = (index) => {
        setActiveIndex(index);
    };


    return (
        <div className='' style={{ height: '100vh' }}>
            <header className='userHeader px-4' style={{ height: '3.5rem' }}>
                <Link to='/' className='link'>
                    <div className="userLogo fs-4 text-dark fw-bold"><span style={{ color: '#0069ec' }}>&lt;</span>MIYAGI<span style={{ color: '#0069ec' }}>&gt;</span></div>
                </Link>
                <h3>Good morning Mo!</h3>
                <input type='search' placeholder='Search' style={{ height: '2rem' }} />
            </header>
            <div className='h-75 d-flex'>
                <div className="optionsBlock pt-1" ref={menuRef}>
                    {menuItem.map((item, index) => (
                        <Link to={item.path} key={index}>
                            <button
                                className={`menuButtonsForUser d-flex align-items-center mt-2 mx-4 ${index === activeIndex ? 'active' : ''}`}
                                onClick={() => handleButtonClick(index)}
                            >
                                <i className={item.icon}></i>
                                <h4 className="m-0 ps-3">{item.name}</h4>
                            </button>
                        </Link>
                    ))}
                    <img src='https://img.freepik.com/free-photo/cheerful-indian-businessman-smiling-closeup-portrait-jobs-career-campaign_53876-129417.jpg?w=740&t=st=1688711539~exp=1688712139~hmac=7ca0f314d9b6432405c3079b05f52e2995ee64726e002d87917ad184e1c7417f'
                        style={{ width: '4rem', height: '4rem', borderRadius: '5rem', marginLeft: '5.5rem', border: '2px solid #fff', marginTop: '0.5rem' }} />
                    <p className='m-0 p-0 text-center text-light'>Mo</p>
                    <p className='m-0 p-0 text-center text-light' style={{ fontSize: '0.8rem' }}>JOINED 2023</p>
                    <div className="container">
                        <div className="progress2 progress-moved">
                            <div className="progress-bar2" >
                            </div>
                        </div>
                    </div>
                    <h6 className='text-center fw-bold'>40 GB available out of 1 TB</h6>
                </div>
                <div className='w-100 m-3 d-flex'>
                    <main className='d-flex flex-column w-100'>{children}</main>
                </div>
                <div className="dropdown1">
                    <button className="dropbtn1">File Requests</button>
                    <ul className="dropdown-content1">
                        <li><a href="#" className='text-dark'>Table Documentation</a></li>
                        <li><a href="#" className='text-dark'>Lorem Ipsum</a></li>
                        <li><a href="#" className='text-dark'>Lorem Ipsum</a></li>
                        <li><a href="#" className='text-dark'>Lorem Ipsum</a></li>
                    </ul>
                </div>
                <div className="dropdown2">
                    <button className="dropbtn2">Transfer Files</button>
                    <ul className="dropdown-content2">
                        <li><a href="#" className='text-dark'>Documentation Miyagi</a></li>
                        <li><a href="#" className='text-dark'>Magical-Retreat-2020.jpg</a></li>
                        <li><a href="#" className='text-dark'>Use-This-Mame.jpg</a></li>
                        <li><a href="#" className='text-dark'>Documentation</a></li>
                    </ul>
                </div>
                <div className="dropdown3">
                    <button className="dropbtn3">Backup On-Chain Data</button>
                    <ul className="dropdown-content3">
                        <li><a href="#" className='text-dark'>Lorem Ipsum</a></li>
                        <li><a href="#" className='text-dark'>Lorem Ipsum</a></li>
                        <li><a href="#" className='text-dark'>Lorem Ipsum</a></li>
                        <li><a href="#" className='text-dark'>Lorem Ipsum</a></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default UserAccount;