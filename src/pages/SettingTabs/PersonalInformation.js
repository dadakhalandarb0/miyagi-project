import './PersonalInformation.css';

const PersonalInformation = () => {
    return (
        // <div className="h-75 border border-danger">
        <div className="">
            <div className="text-center pt-4">
                <img className="rounded-circle border border-primary" style={{ width: '7rem', height: '7rem' }} src="https://img.freepik.com/free-photo/funny-playful-bald-adult-man-squinting-grimacing_176420-18473.jpg?t=st=1689053463~exp=1689054063~hmac=afd55b38e5e71b440b6280bd16c0ff9e0946f801cb7505a17166bf2e21e0f34d" />
            </div>
            <div className="personalDetailsContent">
                <div className="d-flex flex-column">
                Username
                    <button>Yeezxxs.eth</button>
                </div>
                <div className="d-flex flex-column">
                Full Name
                    <button>Farraz Mir</button>
                </div>
                <div className="d-flex flex-column">
                Phone Number
                    <button>9818168895</button>
                </div>
                <div className="d-flex flex-column">
                Email ID
                    <button>mfaraz568@gmail.com</button>
                </div>
                <div className="d-flex flex-column">
                Country
                    <button>India</button>
                </div>
                <div className="d-flex flex-column">
                Industry
                    <button>Tech</button>
                </div>
            </div>
        </div>
        // </div>
    )
}

export default PersonalInformation;