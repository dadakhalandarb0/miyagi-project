import './Interfaces.css';

const Interfaces = () => {
    return (
        <div>
            <h2 className='text-center p-0 m-0' style={{ color: "#2184E5", }}>Change the color theme as per your prefrence</h2>
            <div className="InterfacesContent">
                <div className="d-flex flex-column">
                    <button style={{ backgroundColor: '#004B93' }}>Iris Blue</button>
                </div>
                <div className="d-flex flex-column">
                    <button style={{ backgroundColor: '#CF2850' }}>Viva Magenta</button>
                </div>
                <div className="d-flex flex-column">
                    <button style={{ backgroundColor: '#5BCC80' }}>Emerald Green</button>
                </div>
                <div className="d-flex flex-column">
                    <button style={{ backgroundColor: '#FF4F00' }}>International Orange</button>
                </div>
                <div className="d-flex flex-column">
                    <button style={{ backgroundColor: '#007BEC' }}>Miyagi Blue</button>
                </div>
                <div className="d-flex flex-column">
                    <button style={{ backgroundColor: '#867FDC' }}></button>
                </div>
            </div>
        </div>
    )
}

export default Interfaces;