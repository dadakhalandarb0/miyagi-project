import './BillingInformation.css';

const BillingInformation = () => {
    return (
        <div>
            <div className="BillingInformationContent">
                <div className="d-flex flex-column">
                    Full Name
                    <button></button>
                </div>
                <div className="d-flex flex-column">
                    Billing Address
                    <button></button>
                </div>
                <div className="d-flex flex-column">
                    Payment Method
                    <button></button>
                </div>
                <div className="d-flex flex-column">
                    Credit Card/Debit Card Used
                    <button></button>
                </div>
                <div className="d-flex flex-column">
                    Billing Contact Information
                    <button></button>
                </div>
                <div className="d-flex flex-column">
                    Country
                    <button></button>
                </div>
            </div>
        </div>
    )
}

export default BillingInformation;