import Fade from 'react-reveal/Fade';
import './FrameFour.css';

const FrameFour = () => {
    return (
        <div className="pb-2" style={{ backgroundColor: "#0069ec" }}>
            <p className='m-auto text-center text-light pt-2' style={{ width: '70rem', fontSize: '1.5rem' }}>With the knowledge that your digital belongings will be preserved forever and easily verifiverifiable on the chain, you can enjoy an stress-free and seamless interaction with this timeless technology.</p>
            <Fade bottom delay='2000'>
                <div className="containerReadMore">
                    <div className="btnReadMore"><a href="#" >Read More</a></div>
                </div>
            </Fade>
        </div>
    );
}

export default FrameFour;