import Footer from "../Footer.js";
import FrameFour from "./FrameFour";
import FrameOne from "./FrameOne";
import FrameThree from "./FrameThree";
import FrameTwo from "./FrameTwo";
import Header from "../HeaderComponent";

const FrameComponent = () => {
  return (
    <div>
      <div className="">
        <Header />
      </div>
      <div>
        <FrameOne />
      </div>
      <div>
        <FrameTwo />
      </div>
      <div>
        <FrameThree />
      </div>
      <div>
        <FrameFour />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
};

export default FrameComponent;
