import React from 'react';
import './FrameThree.css';

const FrameThree = () => {

    return (
        <div className='frameThreeContainer'>
            <h2 className='fw-bold ms-4 mt-3' style={{ width: '27rem', fontFamily: "outfit", fontSize: '3rem' }}>What does have to
                &lt;<span style={{ color: '#0069ec' }}>MIYAGI</span>&gt;offer?</h2>
            <div className='d-flex justify-content-end'>
                <img src='1_3Website_content-1.svg' style={{ width: '13rem', marginRight: '5rem' }} />
                <div className='textCarouselFrameThree'>
                    <div>
                        <h2 className='me-4 pt-4' style={{ width: '32rem', fontSize: '4rem', fontFamily: 'outfit' }}><span style={{ color: '#CF2850' }}>Sign </span>Documents using blockchain </h2>
                        <h2 className='' style={{ width: '28rem', fontSize: '1.5rem', color: 'gray' }}>
                            Contract collaboration doesn’t belong
                            in email, documents and chats. With
                            your most important documents stored
                            and managed in one place, you can
                            focus on what matters most.
                        </h2>
                    </div>
                    <div>
                        <h2 className='me-4 pt-4' style={{ width: '32rem', fontSize: '4rem', fontFamily: 'outfit' }}><span style={{ color: '#CF2850' }}>Store </span>Data using blockchain</h2>
                        <h2 className='' style={{ width: '28rem', fontSize: '1.5rem', color: 'gray' }}>
                            Contract collaboration doesn’t belong
                            in email, documents and chats. With
                            your most important documents stored
                            and managed in one place, you can
                            focus on what matters most.
                        </h2>
                    </div>
                    <div>
                        <h2 className='me-4 pt-4' style={{ width: '32rem', fontSize: '4rem', fontFamily: 'outfit' }}><span style={{ color: '#CF2850' }}>Verify </span>Documents using blockchain </h2>
                        <h2 className='' style={{ width: '28rem', fontSize: '1.5rem', color: 'gray' }}>
                            Contract collaboration doesn’t belong
                            in email, documents and chats. With
                            your most important documents stored
                            and managed in one place, you can
                            focus on what matters most.
                        </h2>
                    </div>
                </div>
                <div className="pic-ctn">
                    <img src="https://picsum.photos/200/300?t=1" alt="" className="pic" />
                    <img src="https://picsum.photos/200/300?t=2" alt="" className="pic" />
                    <img src="https://picsum.photos/200/300?t=3" alt="" className="pic" />
                    <div className='m-auto rounded-bottom-5' style={{ width: '7rem', height: '1rem', backgroundColor: 'black' }}></div>
                    <div className='border border-dark hrline' style={{ width: '6rem', }}></div>
                </div>
            </div>
            <div className='h-100' style={{ backgroundColor: '#CF2850' }}>
                <p className='text-center pt-4 fw-bold text-light m-auto' style={{ width: '70rem', fontSize: '1.5rem'}}>
                    Our goal at Miyagi is to make your interactions with this innovative technology as humane as possible and providing you with endless inspiration for your creative journey.
                </p>
            </div>
        </div>
    );
};

export default FrameThree;
