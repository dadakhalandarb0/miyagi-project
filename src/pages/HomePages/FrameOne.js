import { useTypewriter, Cursor } from 'react-simple-typewriter';
import './FrameOne.css';
import { Link } from 'react-router-dom';

const FrameOne = () => {

    const [text] = useTypewriter({
        words: ["Miyagi"],
        loop: {},
        typeSpeed: 150,
        deleteSpeed: 100,
    });

    return (
        <div className="main" style={{ paddingTop: '10rem' }}>
            <img className='cube' src="1_3cube.svg" />
            <div className="bottom"></div>
            <img className='cube1' src="1_3cube.svg" />
            <div className="bottom1"></div>
            <img className='cube2' src="1_3cube.svg" />
            <div className="bottom2"></div>
            <img className='cube3' src="1_3cube.svg" />
            <div className="bottom3"></div>
            <p className="fs-1 text-uppercase"
                style={{ color: 'black', fontFamily: 'outfit', fontWeight: '1000' }}>
                &lt; with{" "}
                <span className="words" style={{ color: '#0069ec' }}>
                    {text}
                </span>
                <Cursor />
                your <br /> documents are <span style={{ color: '#0069ec' }}>safe</span> &gt;
            </p>
            <div>
                <Link to='/varifynow'>
                    <button className="FrameOneButton">
                        Verify Now
                    </button>
                </Link>
                <Link to="/login">
                    <button className="FrameOneButton">
                        Decentralise Now
                    </button>
                </Link>
            </div>
            <div className='w-100 p-3 mb-0 text-center text-uppercase' style={{ backgroundColor: '#0069ec', marginTop: 'auto' }}>
                <p className="fs-3 text-light mb-0">
                    Unlock the power of
                </p>
                <p className="fs-3 text-light fw-bold mb-0">
                    Blockchain
                </p>
            </div>
        </div>
    )
}

export default FrameOne;