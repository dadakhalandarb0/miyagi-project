import styles from './FrameComponent.module.css';
import './FrameTwo.css';
import Rotate from 'react-reveal/Rotate';
import Bounce from 'react-reveal/Bounce';
import Fade from 'react-reveal/Fade';

const FrameTwo = () => {

    return (
        <div className="main-container">
            <div className="carousel-item d-flex flex-column align-items-end" style={{ backgroundColor: '#004B93', paddingTop: '2rem' }}>
                <Bounce top cascade delay='500'>
                    <h1 className='pe-5' style={{ fontSize: '3rem' }}>Welcome to the era of</h1>
                    <h1 className='pe-5 fw-bold text-light' style={{ fontSize: '3rem' }}>Decentralistion</h1>
                </Bounce>
                <Fade bottom>
                    <img
                        className={styles.image0323501Icon}
                        alt=""
                        src="/image0-323-50-1@2x.png"
                    />
                </Fade>
                <Fade right cascade delay='1000'>
                    <p className="text-end fw-bold pe-5 w-50 text-light" style={{ fontSize: '1.3rem' }}>
                        Witness the emergence of a new era, where trust
                        flows freely, collaboration knows no bounds, and the
                        infinite possibilities of human potential unfold in
                        graceful union with the interconnected web of
                        existence.
                    </p>
                </Fade>
                <div>
                    <Rotate top right delay='2000'>
                        <img src="Ellipse 13semi circle.svg"
                            style={{ width: "12rem", height: '15rem' }}
                        />
                    </Rotate>
                </div>
            </div>
        </div>
    )
}


export default FrameTwo;