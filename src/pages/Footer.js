import './Footer.css';

const Footer = () => {
    return (
        <div>
            <footer className="footer">
                <h3 className='text-dark fw-bold'>Decentralise. Simplify. Digitilise.</h3>
                <p className='text-dark w-50'>We’re a diverse and passionate team that takes ownership of your problems and empowers you to execute your plans. We stay light on our feet and truly enjoy delivering great work.</p>
                <ul className="wrapper1 border-bottom">
                    <li className="icon facebook">
                        <span className="tooltip">Facebook</span>
                        <span><i className="fab fa-facebook-f"></i></span>
                    </li>
                    <li className="icon twitter">
                        <span className="tooltip">Twitter</span>
                        <span><i className="fab fa-twitter"></i></span>
                    </li>
                    <li className="icon instagram">
                        <span className="tooltip">Instagram</span>
                        <span><i className="fab fa-instagram"></i></span>
                    </li>
                    <li className="icon github">
                        <span className="tooltip">Github</span>
                        <span><i className="fab fa-github"></i></span>
                    </li>
                    <li className="icon youtube">
                        <span className="tooltip">Youtube</span>
                        <span><i className="fab fa-youtube"></i></span>
                    </li>
                </ul>
                <div className='d-flex align-items-center justify-content-between'
                    style={{ borderRadius: '1rem', width: '60rem', height: '4rem', backgroundColor: '#c2ede0' }}>
                    <h5 className='ps-4 fw-bold text-secondary m-0'>Newsletter Sign Up</h5>
                    <div className="search-box me-2">
                        <button className="btn-search"><i className="fas fa-envelope"></i></button>
                        <input type="text" className="input-search" placeholder="Enter your email" />
                    </div>
                </div>
                <ul className="menu">
                    <li className="menu__item"><a className="menu__link" href="#">Home</a></li>
                    <li className="menu__item"><a className="menu__link" href="#">About Us</a></li>
                    <li className="menu__item"><a className="menu__link" href="#">Contact Us</a></li>
                    <li className="menu__item"><a className="menu__link" href="#">Help</a></li>
                    <li className="menu__item"><a className="menu__link" href="#">Privacy Policy</a></li>
                    <li className="menu__item"><a className="menu__link" href="#">Disclaimer</a></li>
                </ul>
                <p>&copy;2023 MIYAGI LLP. All Rights Reserved</p>
            </footer>
        </div>
    )
}

export default Footer;