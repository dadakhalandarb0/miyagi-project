import { Link, useNavigate } from "react-router-dom";
import "./SignUp.css"
import { getAuth, signInWithPopup, GoogleAuthProvider } from "firebase/auth"
import "../config/firebase-config.js"
import { useState } from "react"

const auth = getAuth()
const provider = new GoogleAuthProvider()

const SignUp = () => {
    const [authorizedUser, setAuthorizedUser] = useState(
        false || sessionStorage.getItem("accessToken")
    )

    const navigate = useNavigate(); // Initialize useNavigate

    function signUpWithGoogle() {
        signInWithPopup(auth, provider)
            .then((result) => {
                // This gives you a Google Access Token. You can use it to access the Google API.
                const credential = GoogleAuthProvider.credentialFromResult(result)
                const token = credential.accessToken
                // The signed-in user info.
                const user = result.user
                // IdP data available using getAdditionalUserInfo(result)
                // ...
                if (user) {
                    user.getIdToken().then((tkn) => {
                        // set access token in session storage
                        sessionStorage.setItem("accessToken", tkn)
                        setAuthorizedUser(true)

                        // Redirect to the dashboard page using useNavigate
                        navigate("/signup/profiledetail");
                    })
                }
            })
            .catch((error) => {
                // Handle Errors here.
                const errorCode = error.code
                const errorMessage = error.message
                // The email of the user's account used.
                const email = error.customData.email
                // The AuthCredential type that was used.
                const credential = GoogleAuthProvider.credentialFromError(error)
                // ...
            })

        //Storing access token in session storage
    }

    return (
        <div className="loginPageMainContainer">
            <img className="cube00" src="1_3cube.svg" />
            <img className="cube11" src="1_3cube.svg" />
            <img className="cube22" src="1_3cube.svg" />
            <img className="circle22" src="Ellipse 13blue.svg" />
            <div className="homebuttonlogo">
                <Link to="/">
                    &lt;<span style={{ color: "black" }}>Miyagi</span>&gt;
                </Link>
            </div>
            <div className="LoginButtonLogo">
                <Link to="/login">
                    &lt;<span style={{ color: "black" }}>LogIn</span>&gt;
                </Link>
            </div>
            <div className="partOne">
                <h2>&lt;SIGN UP&gt;</h2>
                <h6>
                    &lt;WELCOME TO <span>MIYAGI</span>&gt;
                </h6>
                <p className="text-center">
                    By signing up you are agreeing
                    <br />
                    our <span style={{ color: "#0069ec" }}>Term and privacy policy</span>
                </p>
                <div className="row">
                    <span>
                        <input
                            className="basic-slide shadow"
                            id="email"
                            type="text"
                            placeholder="Your favorite email"
                        />
                        <label htmlFor="email">Email</label>
                    </span>
                </div>
                <Link to="/signup/profiledetail">
                    <button className="continueButton">Continue</button>
                </Link>
            </div>
            <div className="partTwo flex-column">
                <h4 className="p-0 m-0 text-light">OR</h4>
                <div className="d-flex">
                    {authorizedUser ? (
                        <></>
                    ) : (
                        <a className="btnfos btnfos d-flex" onClick={signUpWithGoogle}>
                            <img
                                src="icons8-google-logo-48.png"
                                className="ms-2"
                                style={{ width: "2rem", height: "2rem" }}
                            />
                            <p className="m-0 w-75 pt-1">Login with Google</p>
                        </a>
                    )}
                    <a className="btnfos btnfos d-flex">
                        <img
                            src="icons8-apple-logo-50.png"
                            className="ms-2"
                            style={{ width: "2rem", height: "2rem" }}
                        />
                        <p className="m-0 w-75 pt-1">Login with Apple</p>
                    </a>
                </div>
            </div>
        </div>
    )
}

export default SignUp
