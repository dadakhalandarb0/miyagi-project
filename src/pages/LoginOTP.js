import { Link } from 'react-router-dom';
import React, { useRef, useEffect } from 'react';
import './LoginOTP.css';

const LoginOTP = () => {

    const inputRefs = Array.from({ length: 6 }, () => useRef(null));

    const handleInput = (index, e) => {
        if (e.target.value) {
            if (index < inputRefs.length - 1) {
                inputRefs[index + 1].current.focus();
            }
        } else {
            if (index > 0) {
                inputRefs[index - 1].current.focus();
            }
        }
    };

    const handlePaste = (e) => {
        const pasteData = e.clipboardData.getData('text');
        if (pasteData.length === 6) {
            for (let i = 0; i < 6; i++) {
                inputRefs[i].current.value = pasteData[i];
            }
            inputRefs[5].current.focus();
        }
    };

    useEffect(() => {
        inputRefs[0].current.focus();
    }, []);

    return (
        <div className="loginOTPMainContainer">
            <img className='cube00' src="../1_3cube.svg" />
            <img className='cube11' src="../1_3cube.svg" />
            <img className='cube22' src="../1_3cube.svg" />
            <img className='circle22' src='../Ellipse 13blue.svg' />
            <div className='homebuttonlogo'>
                <Link to='/'>
                    &lt;<span style={{ color: 'black' }}>HOME</span>&gt;
                </Link>
            </div>
            <div className='partOneOTP'>
                <h2>&lt;LOGIN&gt;</h2>
                <h6>&lt;WELOCOME <span>BACK</span>&gt;</h6>
                <h6>&lt;OTP HAS BEEN SENT TO YOUR <span>EMAIL ID</span>&gt;</h6>
            </div>
            <div className='partTwoOTP'>
            <h4 className='m-0 p-0 text-light fw-bold'>Enter OTP Here</h4>
                <div className='d-flex flex-row align-items-center'>
                    <div className="otp-container">
                        {Array.from({ length: 6 }).map((_, index) => (
                            <input
                                key={index}
                                type="text"
                                maxLength="1"
                                className="otp-input"
                                ref={inputRefs[index]}
                                onChange={(e) => handleInput(index, e)}
                                onPaste={handlePaste}
                            />
                        ))}
                    </div>
                    <Link to='/user'>
                        <button className="circle-button">
                            <div className="checkmark">
                                <i className="fa-solid fa-circle-check" style={{ color: '#fff' }}></i>
                            </div>
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default LoginOTP;