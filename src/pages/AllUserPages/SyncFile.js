import React from 'react';
import './MyData.css';

const SyncFile = () => {
    return (
        <div className='d-flex h-100'>
            <div className='me-3 rounded'
                style={{ backgroundColor: '#c2e3ed', height: '110%', width:'50rem' }}>
                <div className='h-100'>
                    <p className='m-0 pt-2 ps-3 pb-1 fs-5'>Folders</p>
                    <div className='d-flex mx-3'>
                        <div className='d-flex bg-light rounded align-items-center ps-2 me-3'
                            style={{ boxShadow: "0 0 10px 2px rgba(0, 0, 0, 0.25)", width: '11rem', height: '2.5rem' }}>
                            <i className="fa-solid fa-folder" style={{ color: "#3584e4" }}></i>
                            <p className='m-0 ps-2'>2020 Planning</p>
                        </div>
                        <div className='d-flex bg-light rounded align-items-center ps-2 me-3'
                            style={{ boxShadow: "0 0 10px 2px rgba(0, 0, 0, 0.25)", width: '11rem', height: '2.5rem' }}>
                            <i className="fa-solid fa-folder" style={{ color: "#3584e4" }}></i>
                            <p className='m-0 ps-2'>2020 Planning</p>
                        </div>
                        <div className='d-flex bg-light rounded align-items-center ps-2 me-3'
                            style={{ boxShadow: "0 0 10px 2px rgba(0, 0, 0, 0.25)", width: '11rem', height: '2.5rem' }}>
                            <i className="fa-solid fa-folder" style={{ color: "#3584e4" }}></i>
                            <p className='m-0 ps-2'>2020 Planning</p>
                        </div>
                    </div>
                    <p className='m-0 pt-2 ps-3 py-2'>Files</p>
                    <div className='h-75'>
                        <div className='d-flex justify-content-around'>
                            <p className='m-0 me-5 ps-2' style={{ width: '15rem' }}>Name <i className="fa-solid fa-arrow-up" style={{ color: "#000000" }}></i></p>
                            <p className='m-0 pe-3'>tag </p>
                            <p className='m-0 ps-2 pe-3'>Sync Status </p>
                            <p className='m-0 pe-5'>Share Status </p>
                            <p className='m-0 px-3'>Share Date </p>
                        </div>
                        <div className='pt-3 mydataScrollBarOne'>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-folder" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Documentation Miyagi</span>
                                    <i className="ps-3 fa-regular fa-star" style={{ color: "#3584e4" }}></i>
                                </p>
                                <p className='m-0 px-3 py-1 mx-4 rounded text-center text-light' style={{ width: '4.4rem', backgroundColor: '#004B93BA' }}>Work</p>
                                <p className='m-0 ps-4 pe-4'><i className="fa-solid fa-rotate" style={{ color: "#1c71d8" }}></i></p>
                                <p className='m-0 ps-2'>
                                    <img className='m-0 rounded-circle' style={{ width: '2rem', height: '2rem' }} src='https://img.freepik.com/free-photo/bald-smiling-middle-aged-man-with-beard-looking_176420-18472.jpg?w=740&t=st=1689053463~exp=1689054063~hmac=219501039bdca920dc24c70f2258a32d1641b85bba1ed62183fb526a8e04d0ab' />
                                    <span className='ps-2'>Darlene Robertson</span>
                                </p>
                                <p className='m-0 ps-2'></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-image" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Magical-Retreat-2020.jpg</span>
                                    {/* <i className="ps-3 fa-regular fa-star" style={{ color: "#3584e4" }}></i> */}
                                </p>
                                <p className='m-0 px-3 py-1 mx-4 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#DA708D' }}>File</p>
                                <p className='m-0 ps-4 pe-4'><i className="fa-solid fa-rotate" style={{ color: "#1c71d8" }}></i></p>
                                <p className='m-0 ps-2'>
                                    {/* <img className='m-0 rounded-circle' style={{ width: '2rem', height: '2rem' }} src='#' /> */}
                                    <span className='ps-2'>None</span>
                                </p>
                                <p className='m-0 ps-2'></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-folder" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Use-This-Meme.jpg</span>
                                    {/* <i className="ps-3 fa-regular fa-star" style={{ color: "#3584e4" }}></i> */}
                                </p>
                                <p className='m-0 px-3 py-1 mx-4 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#FAD980' }}>Bills</p>
                                <p className='m-0 ps-4 pe-4'><i className="fa-solid fa-rotate" style={{ color: "#1c71d8" }}></i></p>
                                <p className='m-0 ps-2'>
                                    <img className='m-0 rounded-circle' style={{ width: '2rem', height: '2rem' }} src='https://img.freepik.com/free-photo/wow-check-this-out-surprised-excited-girl-looking-mobile-phone-screen-online-shopping-with-big-discounts-standing-casual-clothes-against-white-wall_176420-34437.jpg?w=740&t=st=1689056396~exp=1689056996~hmac=3886ff5fe7f35f1d5ffac78f885402e155ccf4208acfd55738dd7248c048d3c0' />
                                    <span className='ps-2'>Jane Cooper</span>
                                </p>
                                <p className='m-0 ps-2'></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-file" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Documentation</span>
                                    <i className="ps-3 fa-regular fa-star" style={{ color: "#3584e4" }}></i>
                                </p>
                                <p className='m-0 px-3 py-1 mx-4 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#50C878' }}>File</p>
                                <p className='m-0 ps-4 pe-4'><i className="fa-solid fa-rotate" style={{ color: "#1c71d8" }}></i></p>
                                <p className='m-0 ps-2'>
                                    <img className='m-0 rounded-circle' style={{ width: '2rem', height: '2rem' }} src='https://img.freepik.com/free-photo/cute-freelance-girl-using-laptop-sitting-floor-smiling_176420-20221.jpg?t=st=1689056396~exp=1689056996~hmac=06cf7f325e959b3215b816a6d1bdd1e1f8914c0af549d14b87769841b1d977c8' />
                                    <span className='ps-2'>Theresa Webb</span>
                                </p>
                                <p className='m-0 ps-2'></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-file" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Table Documentation</span>
                                    {/* <i className="ps-3 fa-regular fa-star" style={{ color: "#3584e4" }}></i> */}
                                </p>
                                <p className='m-0 px-3 py-1 mx-4 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#5A4FCFBA' }}>Video</p>
                                <p className='m-0 ps-4 pe-4'>
                                    {/* <i className="fa-solid fa-rotate" style={{ color: "#1c71d8" }}></i> */}
                                </p>
                                <p className='m-0 ps-2'>
                                    <img className='m-0 rounded-circle' style={{ width: '2rem', height: '2rem' }} src='https://img.freepik.com/free-photo/bald-smiling-middle-aged-man-with-beard-looking_176420-18472.jpg?w=740&t=st=1689053463~exp=1689054063~hmac=219501039bdca920dc24c70f2258a32d1641b85bba1ed62183fb526a8e04d0ab' />
                                    <span className='ps-2'>Hash Code</span>
                                </p>
                                <p className='m-0 ps-2'></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-image" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Magical-Retreat-2020.jpg</span>
                                    {/* <i className="ps-3 fa-regular fa-star" style={{ color: "#3584e4" }}></i> */}
                                </p>
                                <p className='m-0 px-3 py-1 mx-4 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#DA708D' }}>File</p>
                                <p className='m-0 ps-4 pe-4'><i className="fa-solid fa-rotate" style={{ color: "#1c71d8" }}></i></p>
                                <p className='m-0 ps-2'>
                                    <img className='m-0 rounded-circle' style={{ width: '2rem', height: '2rem' }} src='https://img.freepik.com/free-photo/bald-smiling-middle-aged-man-with-beard-looking_176420-18472.jpg?w=740&t=st=1689053463~exp=1689054063~hmac=219501039bdca920dc24c70f2258a32d1641b85bba1ed62183fb526a8e04d0ab' />
                                    <span className='ps-2'>Darlene Robertson</span>
                                </p>
                                <p className='m-0 ps-2'></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-image" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Magical-Retreat-2020.jpg</span>
                                    {/* <i className="ps-3 fa-regular fa-star" style={{ color: "#3584e4" }}></i> */}
                                </p>
                                <p className='m-0 px-3 py-1 mx-4 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#DA708D' }}>File</p>
                                <p className='m-0 ps-4 pe-4'><i className="fa-solid fa-rotate" style={{ color: "#1c71d8" }}></i></p>
                                <p className='m-0 ps-2'>
                                    <img className='m-0 rounded-circle' style={{ width: '2rem', height: '2rem' }} src='https://img.freepik.com/free-photo/bald-smiling-middle-aged-man-with-beard-looking_176420-18472.jpg?w=740&t=st=1689053463~exp=1689054063~hmac=219501039bdca920dc24c70f2258a32d1641b85bba1ed62183fb526a8e04d0ab' />
                                    <span className='ps-2'>Darlene Robertson</span>
                                </p>
                                <p className='m-0 ps-2'></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='d-flex flex-column p-2'
                style={{ height: '110%' }}>
                <h6 className='m-0 text-center'>Upload data to Blockchain</h6>
                <div className='shadows d-flex justify-content-center flex-column align-items-center fs-3 text-primary'
                    style={{
                        width: '14rem',
                        borderRadius: '0.7rem',
                        height: '10rem',
                        backgroundColor: '#c2e3ed',
                        cursor: 'pointer',
                    }}>
                    <i className="fa-solid fa-upload"></i>
                    <span>Drag & Drop</span>
                </div>
                <h6 className='m-0 text-center pt-1'>Sync</h6>
                <div className='shadows d-flex justify-content-center flex-column align-items-center fs-3 text-primary'
                    style={{
                        width: '14rem',
                        borderRadius: '0.7rem',
                        height: '10rem',
                        backgroundColor: '#c2e3ed',
                        cursor: 'pointer',
                    }}>
                    <i className="fa-solid fa-rotate"></i>
                    <span>Drag & Drop</span>
                </div>
                <h6 className='m-0 text-center pt-1'>Timestamp/E-Sign Document</h6>
                <div className='shadows d-flex justify-content-center flex-column align-items-center fs-3 text-primary'
                    style={{
                        width: '14rem',
                        borderRadius: '0.7rem',
                        height: '10rem',
                        backgroundColor: '#c2e3ed',
                        cursor: 'pointer',
                    }}>
                    <img src='/vector.svg' style={{ width: '2.5rem' }} />
                    Drag & Drop
                </div>
            </div>
        </div>
    );
};

export default SyncFile;