import React from 'react';
import './Trash.css';

const Trash = () => {
    return (
        <div className='d-flex h-100'>
            <div className='me-3 rounded'
                style={{ backgroundColor: '#c2e3ed', height: '110%', width:'50rem' }}>
                <div className='h-100'>
                    <div className='h-100 pt-3'>
                        <div className='d-flex justify-content-between'>
                            <p className='m-0 me-5 ps-4' style={{ width: '15rem' }}>Name <i className="fa-solid fa-arrow-up" style={{ color: "#000000" }}></i></p>
                            <p className='m-0 ps-5'>tag </p>
                            <p className='m-0 px-5'>Delete Date </p>
                        </div>
                        <div className='pt-3 TrashScrollBarOne'>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center justify-content-between'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-folder" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Documentation Miyagi</span>
                                </p>
                                <p className='m-0 px-3 py-1 rounded text-center text-light' style={{ width: '4.4rem', backgroundColor: '#004B93BA' }}>Work</p>
                                <p className='m-0 border me-4' style={{ width: '5rem' }}></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center justify-content-between'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-image" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Magical-Retreat-2020.jpg</span>
                                </p>
                                <p className='m-0 px-3 py-1 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#DA708D' }}>File</p>
                                <p className='m-0 border me-4' style={{ width: '5rem' }}></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center justify-content-between'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-folder" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Use-This-Meme.jpg</span>
                                </p>
                                <p className='m-0 px-3 py-1 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#FAD980' }}>Bills</p>
                                <p className='m-0 border me-4' style={{ width: '5rem' }}></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center justify-content-between justify-content-between'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-file" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Documentation</span>
                                </p>
                                <p className='m-0 px-3 py-1 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#50C878' }}>File</p>
                                <p className='m-0 border me-4' style={{ width: '5rem' }}></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center justify-content-between'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-file" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Table Documentation</span>
                                </p>
                                <p className='m-0 px-3 py-1 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#5A4FCFBA' }}>Video</p>
                                <p className='m-0 border me-4' style={{ width: '5rem' }}></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center justify-content-between'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-image" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Magical-Retreat-2020.jpg</span>
                                </p>
                                <p className='m-0 px-3 py-1 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#DA708D' }}>File</p>
                                <p className='m-0 border me-4' style={{ width: '5rem' }}></p>
                            </div>
                            <div className='d-flex mb-2 mx-2 bg-light p-2 rounded align-items-center justify-content-between'>
                                <p className='m-0 ps-2' style={{ width: '16rem' }}>
                                    <i className="pe-3 fa-solid fa-image" style={{ color: "#1c71d8" }}></i>
                                    <span className='d-inline-block text-truncate' style={{ maxWidth: '163px' }}>Magical-Retreat-2020.jpg</span>
                                </p>
                                <p className='m-0 px-3 py-1 rounded text-light text-center' style={{ width: '4.4rem', backgroundColor: '#DA708D' }}>File</p>
                                <p className='m-0 border me-4' style={{ width: '5rem' }}></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='d-flex flex-column p-2'
                style={{ height: '110%' }}>
                <h6 className='m-0 text-center pb-2'>Recover</h6>
                <div className='shadows d-flex justify-content-center flex-column align-items-center fs-3 text-primary'
                    style={{
                        width: '14rem',
                        borderRadius: '0.7rem',
                        height: '13rem',
                        backgroundColor: '#c2e3ed',
                        cursor: 'pointer',
                    }}>
                    <i className="fa-solid fa-folder" style={{color: "#1c71d8"}}></i>
                    <span>Drag & Drop</span>
                </div>
                <h6 className='m-0 text-center pt-3 pb-2'>Permenenatly Delete</h6>
                <div className='shadows d-flex justify-content-center flex-column align-items-center fs-3 text-primary'
                    style={{
                        width: '14rem',
                        borderRadius: '0.7rem',
                        height: '13rem',
                        backgroundColor: '#c2e3ed',
                        cursor: 'pointer',
                    }}>
                    <i className="fa-solid fa-trash"></i>
                    <span>Drag & Drop</span>
                </div>
            </div>
        </div>
    );
};

export default Trash;