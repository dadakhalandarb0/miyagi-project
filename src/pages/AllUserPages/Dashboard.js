import React, { useState } from 'react';
import './Dashboard.css';

const Dashboard = () => {
    const [blockChainFile, setBlockChainFile] = useState(null);
    const [timeStampFile, setTimeStampFile] = useState(null);

    const handleBlockChain = (event) => {
        const file = event.target.files[0];
        setBlockChainFile(file);
    };

    const handleTimeStamp = (event) => {
        const file = event.target.files[0];
        setTimeStampFile(file);
    };

    return (
        <>
            <div className=' d-flex flex-column'>
                <div className='d-flex justify-content-around'>
                    <h6 className='m-0'>
                        Upload to Decentralise
                    </h6>
                    <h6 className='m-0'>
                        Upload to the chain
                    </h6>
                    <h6 className='m-0'>
                        Recently accessed folders
                    </h6>
                </div>
                {/* blockchain File upload */}
                <div className='d-flex justify-content-around'>
                    <label
                        className="file-uploader d-flex justify-content-center align-items-center fs-3 text-primary"
                        htmlFor="fileInput"
                        style={{
                            width: '20rem',
                            height: '15rem',
                            borderRadius: '0.7rem',
                            backgroundColor: blockChainFile ? '#c2e3ed' : '#c2e3ed',
                            cursor: 'pointer',
                        }}
                    >
                        {blockChainFile ? (
                            <div>
                                <i className="fa-solid fa-file"></i><br />
                                <span>{blockChainFile.name}</span>
                            </div>
                        ) : (
                            <div>
                                <i className="fa-solid fa-upload"></i><br />
                                <span>Drag & Drop</span>
                            </div>
                        )}
                        <input
                            type="file"
                            id="fileInput"
                            accept=".pdf"
                            style={{ display: 'none' }}
                            onChange={handleBlockChain}
                        />
                    </label>
                    {/* upload timestamp file */}
                    <label
                        className="file-uploader1 d-flex justify-content-center align-items-center fs-3 text-primary"
                        htmlFor="fileInput1"
                        style={{
                            width: '20rem',
                            height: '15rem',
                            borderRadius: '0.7rem',
                            backgroundColor: timeStampFile ? '#c2e3ed' : '#c2e3ed',
                            cursor: 'pointer',
                        }}
                    >
                        {timeStampFile ? (
                            <div>
                                <i className="fa-solid fa-file"></i><br />
                                <span>{timeStampFile.name}</span>
                            </div>
                        ) : (
                            <div>
                                <img src='/vector.svg' style={{ width: '2.5rem' }} /><br />
                                <span>Drag & Drop</span>
                            </div>
                        )}
                        <input
                            type="file"
                            id="fileInput1"
                            accept=".pdf"
                            style={{ display: 'none' }}
                            onChange={handleTimeStamp}
                        />
                    </label>
                    <div className='shadows'
                        style={{
                            width: '20rem',
                            borderRadius: '0.7rem',
                            height: '15rem',
                            backgroundColor: '#c2e3ed',
                            cursor: 'pointer'
                        }}>
                        <div className='d-flex pt-3'>
                            <p className='pe-5 ps-3'>File</p>
                            <p className='ps-4 pe-2'>Size</p>
                            <p className='ps-4 pe-3'>Tag</p>
                            <p className='ps-1'>Accessed By</p>
                        </div>
                        <hr className='mt-0 mb-2 mx-4' style={{ width: '17rem', border: '1px solid #000' }} />
                        <div className='folderList'>
                            <div className='d-flex mb-3'>
                                <div className='d-flex' style={{ width: '6rem' }}>
                                    <i className="fa-solid fa-file ps-2" style={{ color: "#3584e4" }}></i>
                                    <p className='m-0 ps-1' style={{ fontSize: '0.7rem' }}>Black Power By 2PAC.MP4</p>
                                </div>
                                <p className='m-0 ps-3' style={{ fontSize: '0.7rem' }}>2 MB</p>
                                <div className='m-0 ms-4 text-center p-1 text-light'
                                    style={{
                                        fontSize: '0.7rem',
                                        backgroundColor: 'rgba(126, 36, 57, 0.73)',
                                        width: '3rem', height: '1.2rem',
                                        borderRadius: '1rem'
                                    }}>Music</div>
                                <p className='m-0 ps-4' style={{ fontSize: '0.8rem' }}>Mail List</p>
                            </div>
                            <div className='d-flex mb-3'>
                                <div className='d-flex' style={{ width: '6rem' }}>
                                    <i className="fa-solid fa-file ps-2" style={{ color: "#3584e4" }}></i>
                                    <p className='m-0 ps-1' style={{ fontSize: '0.7rem' }}>Dna based Data storage blueprint.file</p>
                                </div>
                                <p className='m-0 ps-3' style={{ fontSize: '0.7rem' }}>2 MB</p>
                                <div className='m-0 ms-4 text-center p-1 text-light'
                                    style={{
                                        fontSize: '0.7rem',
                                        backgroundColor: 'rgba(0, 75, 147, 0.73)',
                                        width: '3rem', height: '1.2rem',
                                        borderRadius: '1rem'
                                    }}>Work</div>
                                <p className='m-0 ps-4' style={{ fontSize: '0.8rem' }}>No one</p>
                            </div>
                            <div className='d-flex mb-3'>
                                <div className='d-flex' style={{ width: '6rem' }}>
                                    <i className="fa-solid fa-file ps-2" style={{ color: "#3584e4" }}></i>
                                    <p className='m-0 ps-1' style={{ fontSize: '0.7rem' }}>Power to the people by John Lenon.MP4</p>
                                </div>
                                <p className='m-0 ps-3' style={{ fontSize: '0.7rem' }}>2 MB</p>
                                <div className='m-0 ms-4 text-center p-1 text-light'
                                    style={{
                                        fontSize: '0.7rem',
                                        backgroundColor: 'rgba(126, 36, 57, 0.73)',
                                        width: '3rem', height: '1.2rem',
                                        borderRadius: '1rem'
                                    }}>Music</div>
                                <p className='m-0 ps-4' style={{ fontSize: '0.8rem' }}>Mail List</p>
                            </div>
                            <div className='d-flex mb-3'>
                                <div className='d-flex' style={{ width: '6rem' }}>
                                    <i className="fa-solid fa-file ps-2" style={{ color: "#3584e4" }}></i>
                                    <p className='m-0 ps-1' style={{ fontSize: '0.7rem' }}>Hoverboard recipt.file</p>
                                </div>
                                <p className='m-0 ps-3' style={{ fontSize: '0.7rem' }}>2 MB</p>
                                <div className='m-0 ms-4 text-center p-1 text-light'
                                    style={{
                                        fontSize: '0.7rem',
                                        backgroundColor: 'rgba(255, 206, 81, 0.73)',
                                        width: '3rem', height: '1.2rem',
                                        borderRadius: '1rem'
                                    }}>Bills</div>
                                <p className='m-0 ps-4' style={{ fontSize: '0.8rem' }}>Mom</p>
                            </div>
                            <div className='d-flex mb-3'>
                                <div className='d-flex' style={{ width: '6rem' }}>
                                    <i className="fa-solid fa-file ps-2" style={{ color: "#3584e4" }}></i>
                                    <p className='m-0 ps-1' style={{ fontSize: '0.7rem' }}>Finance report office.pdf</p>
                                </div>
                                <p className='m-0 ps-3' style={{ fontSize: '0.7rem' }}>2 MB</p>
                                <div className='m-0 ms-4 text-center p-1 text-light'
                                    style={{
                                        fontSize: '0.7rem',
                                        backgroundColor: '#50C878',
                                        width: '3rem', height: '1.2rem',
                                        borderRadius: '1rem'
                                    }}>Office</div>
                                <p className='m-0 ps-4' style={{ fontSize: '0.8rem' }}>Mail List</p>
                            </div>
                            <div className='d-flex mb-3'>
                                <div className='d-flex' style={{ width: '6rem' }}>
                                    <i className="fa-solid fa-file ps-2" style={{ color: "#3584e4" }}></i>
                                    <p className='m-0 ps-1' style={{ fontSize: '0.7rem' }}>Black Power By 2PAC.MP4</p>
                                </div>
                                <p className='m-0 ps-3' style={{ fontSize: '0.7rem' }}>2 MB</p>
                                <div className='m-0 ms-4 text-center p-1 text-light'
                                    style={{
                                        fontSize: '0.7rem',
                                        backgroundColor: 'rgba(126, 36, 57, 0.73)',
                                        width: '3rem', height: '1.2rem',
                                        borderRadius: '1rem'
                                    }}>Music</div>
                                <p className='m-0 ps-4' style={{ fontSize: '0.8rem' }}>Mail List</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='d-flex'>
                <div className='d-flex flex-column'>
                    <div className='d-flex'>
                        <div className='Hash-id'>
                            <h6 className='m-0 ps-4 pt-1'>Verify Document Through Hash ID</h6>
                            <div className='text-primary ms-3 shadows'
                                style={{
                                    width: '32rem',
                                    height: '3rem',
                                    backgroundColor: '#c2e3ed',
                                    borderRadius: '0.8rem',
                                    padding: '1rem',
                                    textAlign: 'center',
                                    textTransform: 'uppercase',
                                    fontWeight: 'bold',
                                    fontSize: '1.2rem'
                                }}>
                                miyagi fily id
                            </div>
                        </div>
                        <div className=''>
                            <h6 className='m-0 text-center pt-1'>Deltasafe</h6>
                            <div className='text-primary ms-3 shadows'
                                style={{
                                    width: '14rem',
                                    height: '3rem',
                                    backgroundColor: '#c2e3ed',
                                    borderRadius: '0.8rem',
                                    padding: '0rem',
                                    textAlign: 'center',
                                    textTransform: 'uppercase',
                                    fontWeight: 'bold',
                                }}>
                                <img src='/vector3.svg' style={{ width: '2.5rem', height: '3rem' }} />
                            </div>
                        </div>
                    </div>
                    <div className='h-100 d-flex flex-column'>
                        <h6 className='m-0 ms-3 pt-1 w-100'>Data sort as per file type</h6>
                        <div className='d-flex h-100 pt-1'>
                            <div className='mx-2 h-100 shadows'
                                style={{
                                    borderRadius: '1rem',
                                    backgroundColor: '#c2e3ed',
                                    position: 'relative',
                                    width: '13rem'
                                }}>
                                <div className='d-flex p-2 ps-3'>
                                    <i className="fa-solid fa-image"></i>
                                    <p className='m-0 ps-2 fw-bold'>Images</p>
                                </div>
                                <div className='parallelShape' style={{ position: 'absolute', zIndex: '1', top: '0rem' }}>
                                    <p className='m-0 fw-light text-end pe-4 pt-2' style={{ fontSize: '0.9rem' }}>800 files</p>
                                    <div className=''>
                                        <p className='m-0 pt-4 ps-3'>Shared with:</p>
                                        <img src='https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg?w=740&t=st=1688656824~exp=1688657424~hmac=46328a7a53a7569139124e67471f5b8583cc800d09c2eba6387fd41dc4807ecf'
                                            className='mt-1 ms-3 rounded-circle'
                                            style={{ width: '3rem', height: '3rem', position: 'relative' }} />
                                        <img src='https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg?w=740&t=st=1688656824~exp=1688657424~hmac=46328a7a53a7569139124e67471f5b8583cc800d09c2eba6387fd41dc4807ecf'
                                            className='mt-1 rounded-circle'
                                            style={{ width: '3rem', height: '3rem', position: 'absolute', left: '45px' }} />
                                        <img src='https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg?w=740&t=st=1688656824~exp=1688657424~hmac=46328a7a53a7569139124e67471f5b8583cc800d09c2eba6387fd41dc4807ecf'
                                            className='mt-1 rounded-circle'
                                            style={{ width: '3rem', height: '3rem', position: 'absolute', left: '70px' }} />
                                        <div
                                            className='mt-1 rounded-circle text-center p-3'
                                            style={{ width: '3rem', height: '3rem', position: 'absolute', top: '67px', left: '95px', backgroundColor: '#DEF0FC' }} >+9</div>
                                    </div>
                                </div>
                            </div>
                            <div className='h-100 mx-2 shadows'
                                style={{
                                    borderRadius: '1rem',
                                    backgroundColor: '#c2e3ed',
                                    position: 'relative',
                                    width: '13rem'
                                }}>
                                <div className='d-flex p-2 ps-3'>
                                    <i className="fa-solid fa-video"></i>
                                    <p className='m-0 ps-2 fw-bold'>Video</p>
                                </div>
                                <div className='parallelShape' style={{ position: 'absolute', zIndex: '1', top: '0rem' }}>
                                    <p className='m-0 fw-light text-end pe-4 pt-2' style={{ fontSize: '0.9rem' }}>13 files</p>
                                </div>
                            </div>
                            <div className='h-100 mx-2 shadows'
                                style={{
                                    borderRadius: '1rem',
                                    backgroundColor: '#c2e3ed',
                                    position: 'relative',
                                    width: '13rem'
                                }}>
                                <div className='d-flex p-2 ps-3'>
                                    <i className="fa-solid fa-microphone"></i>
                                    <p className='m-0 ps-2 fw-bold'>Audio</p>
                                </div>
                                <div className='parallelShape' style={{ position: 'absolute', zIndex: '1', top: '0rem' }}>
                                    <p className='m-0 fw-light text-end pe-4 pt-2' style={{ fontSize: '0.9rem' }}>80 files</p>
                                    <div className=''>
                                        <p className='m-0 pt-4 ps-3'>Shared with:</p>
                                        <img src='https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg?w=740&t=st=1688656824~exp=1688657424~hmac=46328a7a53a7569139124e67471f5b8583cc800d09c2eba6387fd41dc4807ecf'
                                            className='mt-1 ms-3 rounded-circle'
                                            style={{ width: '3rem', height: '3rem', position: 'relative' }} />
                                        <img src='https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg?w=740&t=st=1688656824~exp=1688657424~hmac=46328a7a53a7569139124e67471f5b8583cc800d09c2eba6387fd41dc4807ecf'
                                            className='mt-1 rounded-circle'
                                            style={{ width: '3rem', height: '3rem', position: 'absolute', left: '45px' }} />
                                        <img src='https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg?w=740&t=st=1688656824~exp=1688657424~hmac=46328a7a53a7569139124e67471f5b8583cc800d09c2eba6387fd41dc4807ecf'
                                            className='mt-1 rounded-circle'
                                            style={{ width: '3rem', height: '3rem', position: 'absolute', left: '70px' }} />
                                        <div
                                            className='mt-1 rounded-circle text-center p-3'
                                            style={{ width: '3rem', height: '3rem', position: 'absolute', top: '67px', left: '95px', backgroundColor: '#DEF0FC' }} >+1</div>
                                    </div>
                                </div>
                            </div>
                            <div className='shadows h-100 mx-2'
                                style={{
                                    borderRadius: '1rem',
                                    backgroundColor: '#c2e3ed',
                                    position: 'relative',
                                    width: '5rem'
                                }}>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <h6 className='m-0 text-center pt-1'>Recently Timestamped files</h6>
                    <div className='text-primary ms-3 shadows'
                        style={{
                            width: '16rem',
                            height: '15rem',
                            backgroundColor: '#c2e3ed',
                            borderRadius: '0.2rem',
                            padding: '0rem',
                            textAlign: 'center',
                            textTransform: 'uppercase',
                            fontWeight: 'bold',
                        }}>
                        <div className='d-flex text-dark justify-content-around'>
                            <p className='m-0 pt-3' style={{ fontSize: '0.7rem' }}>Docu. Type</p>
                            <p className='m-0 pt-3' style={{ fontSize: '0.7rem' }}>Person</p>
                            <p className='m-0 pt-3' style={{ fontSize: '0.7rem' }}>Organisation</p>
                        </div>
                        <hr className='mx-2 text-dark border-2' />
                        <div>
                            <div className='d-flex text-light justify-content-around py-2'>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(207, 40, 80, 0.65)', borderRadius: '1rem' }}>NDA</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(43, 129, 210, 0.63)', borderRadius: '1rem' }}> Jaideep</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: '#50C878', borderRadius: '1rem' }}>Miyagi</p>
                            </div>
                            <div className='d-flex text-light justify-content-around py-2'>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(207, 40, 80, 0.65)', borderRadius: '1rem' }}>NDA</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(43, 129, 210, 0.63)', borderRadius: '1rem' }}>Isaiah</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: '#50C878', borderRadius: '1rem' }}>Miyagi</p>
                            </div>
                            <div className='d-flex text-light justify-content-around py-2'>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(207, 40, 80, 0.65)', borderRadius: '1rem' }}>NDA</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(43, 129, 210, 0.63)', borderRadius: '1rem' }}>Firas</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: '#50C878', borderRadius: '1rem' }}>Miyagi</p>
                            </div>
                            <div className='d-flex text-light justify-content-around py-2'>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(207, 40, 80, 0.65)', borderRadius: '1rem' }}>NDA</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(43, 129, 210, 0.63)', borderRadius: '1rem' }}>Dada</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: '#50C878', borderRadius: '1rem' }}>Miyagi</p>
                            </div>
                            <div className='d-flex text-light justify-content-around py-2'>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(207, 40, 80, 0.65)', borderRadius: '1rem' }}>NDA</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: 'rgba(43, 129, 210, 0.63)', borderRadius: '1rem' }}>Imaad</p>
                                <p className='m-0 h-25 py-1 px-2 fw-light shadows' style={{ fontSize: '0.7rem', backgroundColor: '#50C878', borderRadius: '1rem' }}>Miyagi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Dashboard;