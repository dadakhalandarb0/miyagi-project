import { useState } from 'react';
import './Settings.css';
import PersonalInformation from '../SettingTabs/PersonalInformation';
import LoginSecurity from '../SettingTabs/LoginSecurity';
import BillingInformation from '../SettingTabs/BillingInformation';
import Interfaces from '../SettingTabs/Interfaces';
import BlockAnalytics from '../SettingTabs/BlockAnalytics';

const Settings = () => {
    const [activeTab, setActiveTab] = useState('personalInformation'); // Set the default active tab to 'personalInformation'

    const handleTabClick = (tabName) => {
        setActiveTab(tabName);
    };

    return (
        <div className="rounded" style={{ height: '85%', backgroundColor: 'rgba(0, 123, 236, 0.07)' }}>
            <ul className="listOfSettingNames">
                <li
                    className={activeTab === 'personalInformation' ? 'active' : ''}
                    onClick={() => handleTabClick('personalInformation')}
                >
                    Personal Information
                </li>
                <li
                    className={activeTab === 'loginSecurity' ? 'active' : ''}
                    onClick={() => handleTabClick('loginSecurity')}
                >
                    Login & Security
                </li>
                <li
                    className={activeTab === 'billingInformation' ? 'active' : ''}
                    onClick={() => handleTabClick('billingInformation')}
                >
                    Billing Information
                </li>
                <li
                    className={activeTab === 'interfaces' ? 'active' : ''}
                    onClick={() => handleTabClick('interfaces')}
                >
                    Interface
                </li>
                <li
                    className={activeTab === 'blockAnalytics' ? 'active' : ''}
                    onClick={() => handleTabClick('blockAnalytics')}
                >
                    Block-Analytics
                </li>
            </ul>
            <div className='h-75'>
                {activeTab === 'personalInformation' && <div><PersonalInformation /></div>}
                {activeTab === 'loginSecurity' && <div><LoginSecurity /></div>}
                {activeTab === 'billingInformation' && <div><BillingInformation /></div>}
                {activeTab === 'interfaces' && <div><Interfaces /></div>}
                {activeTab === 'blockAnalytics' && <div><BlockAnalytics /></div>}
            </div>
        </div>
    );
};

export default Settings;
