import { useState } from 'react';
import './ServicesFrameOne.css';

const ServicesFrameOne = ({ setSelectedWord }) => {

    const wordsArray = ['Education', 'Retail', 'Web 3.0', 'Fintech', 'Healthcare', 'Non-Profit', 'Legal'];

    const [currentIndex, setCurrentIndex] = useState(0);

    const goToNextWord = () => {
        const newIndex = (currentIndex + 1) % wordsArray.length;
        setCurrentIndex(newIndex);
        setSelectedWord(wordsArray[newIndex]);
    };

    const goToPrevWord = () => {
        const newIndex = (currentIndex - 1 + wordsArray.length) % wordsArray.length;
        setCurrentIndex(newIndex);
        setSelectedWord(wordsArray[newIndex]);
    };

    return (
        <div className="ServicesContainerOne">
            <div className='TitlesOfServices'>
                <img className='cubeImageservice' src='1_3cube.svg' />
                <h4>
                    &lt;For <span>{wordsArray[currentIndex]}</span>&gt;
                    <div className="carousel-buttons-Titles">
                        <button onClick={goToPrevWord}><i className="fa-solid fa-arrow-left"></i></button>
                        <div className="points-Titles">
                            {wordsArray.map((word, index) => (
                                <div
                                    key={index}
                                    className={`point-Title ${index === currentIndex ? 'active-Title' : ''}`}
                                ></div>
                            ))}
                        </div>
                        <button onClick={goToNextWord}><i className="fa-solid fa-arrow-right"></i></button>
                    </div>
                </h4>
            </div>
            <div className='OurServiceTextContents'>
                <div>
                    <h4>&lt;Our <span>Services</span>&gt;</h4>
                    <p>
                        &lt; Our Team at <span className='fw-bold'>MIYAGI</span> has come up with some innovative ideas for different sectors that<br />
                        play a major role in the world economy, use the crousel to select your indusry and see<br />
                        what we can do for you.<br />
                        Just a slight reminder, We have not completed the list of industries that we can offer our<br />
                        servies to. So if you belong from an industyr that is not listed here please send us a mail,<br />
                        we would be more than happy to help you adopt Blockchain to your industy and make<br />
                        the Economy more seamless and trust-less &gt;
                    </p>
                </div>
                <img className='EllipseBlueServices' src='Ellipse 13blue.svg' />
            </div>
        </div>
    )
}

export default ServicesFrameOne;