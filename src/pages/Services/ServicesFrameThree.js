import './ServicesFrameThree.css';

const ServicesFrameThree = () => {

    return (
        <div className="ServicesFrameThreeContainer">
            <div className='FrameContentOne'>
                <img src='Ellipse 13blue.svg' />
                <p>
                    From contracts to transactions, every iota of data etches its<br />
                    legacy onto the indelible ledger of the blockchain. It's akin to a<br />
                    digital time capsule, immune to the eroding effects of time. Now,<br />
                    consider the concept of time-signing through blockchain-based<br />
                    e-signatures. It's more than just a signature; it's a stamp that<br />
                    echoes across time, forever validating the 'when' of every<br />
                    agreement.
                </p>
            </div>
            <div className='FrameContentTwo'>
                <p>
                    From contracts to transactions, every iota of data etches its<br />
                    legacy onto the indelible ledger of the blockchain. It's akin to a<br />
                    digital time capsule, immune to the eroding effects of time. Now,<br />
                    consider the concept of time-signing through blockchain-based<br />
                    e-signatures. It's more than just a signature; it's a stamp that<br />
                    echoes across time, forever validating the 'when' of every<br />
                    agreement.
                </p>
                <img src='1_3cube.svg' />
            </div>
        </div>
    )
}

export default ServicesFrameThree;