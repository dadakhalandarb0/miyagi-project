import { useEffect, useState } from 'react';
import './ServicesFrameTwo.css';

const ServicesFrameTwo = ({ selectedWord }) => {

    const ArrayOfEducation = ['Student records', 'Decentraised Storage', 'E-Stamp', 'E-Certificates'];
    const ArrayOfRetail = ['Blockchain-Based Digital Identity for customers', 'Blockchain-Based Product Authentication', 'Decentrlaised Storage', 'Blockchain-Based Product Warranty Authentication'];
    const ArrayOfWeb3 = ['E-Stamp', 'Decentraised Storage'];
    const ArrayOfFintech = ['Blockchain-Based KYC', 'Blockchain-Based Audits', 'Decentrlaised Storage', 'Smart Contracts', 'E-Stamp'];
    const ArrayOfHealthcare = ['Blockchain-Based EHR', 'Blockchain-Based Personal health records', 'Blockchain-Based Medical Bills', 'Decentrlaised Storage', 'Blockchain-Based Health Insurance verification'];
    const ArrayOfNonProfit = ['Blockchain-Based Case Verification', 'Decentrlaised Storage', 'E-Stamp'];
    const ArrayOfLegal = ['Blockchain-Based Legal Certification', 'Blockchain-Based Intelectual property protection', 'Decentralized Legal Records', 'E-Stamp', 'Blockchain-Based Immutable Ledger'];

    let selectedArray = [];

    if (selectedWord === 'Education') {
        selectedArray = ArrayOfEducation;
    } else if (selectedWord === 'Retail') {
        selectedArray = ArrayOfRetail;
    } else if (selectedWord === 'Web 3.0') {
        selectedArray = ArrayOfWeb3;
    } else if (selectedWord === 'Fintech') {
        selectedArray = ArrayOfFintech;
    } else if (selectedWord === 'Healthcare') {
        selectedArray = ArrayOfHealthcare;
    } else if (selectedWord === 'Non-Profit') {
        selectedArray = ArrayOfNonProfit;
    } else if (selectedWord === 'Legal') {
        selectedArray = ArrayOfLegal;
    }

    const [currentIndex, setCurrentIndex] = useState(0);

    const goToNextWord = () => {
        setCurrentIndex((prevIndex) => (prevIndex + 1) % selectedArray.length);
    };

    const goToPrevWord = () => {
        setCurrentIndex((prevIndex) => (prevIndex - 1 + selectedArray.length) % selectedArray.length);
    };

    return (
        <div className="ServicesFrameTwoContainer">
            <div className='PointsUnderTitle'>
                <h4>&lt; {selectedArray[currentIndex]} &gt;</h4>
                <div className="carousel-buttons">
                    <button onClick={goToPrevWord}><i className="fa-solid fa-arrow-left"></i></button>
                    <div className="points">
                        {selectedArray.map((word, index) => (
                            <div
                                key={index}
                                className={`point ${index === currentIndex ? 'active' : ''}`}
                            ></div>
                        ))}
                    </div>
                    <button onClick={goToNextWord}><i className="fa-solid fa-arrow-right"></i></button>
                </div>
            </div>
            <div className='PointsContentUnderTitle'>
                <p>Picture a future where the realm of finance breaks free from the chains of convention. We're<br />
                    embarking on a journey that empowers both individuals and businesses on an unparalleled scale.<br />
                    Imagine a blockchain-based decentralized storage platform as the guardian of your financial world,<br />
                    where security and transparency reign supreme.
                    <br /><br />
                    Envision a digital vault distributed across a network of nodes, where your financial data rests under<br />
                    your sole control, impervious to external threats. This isn't just about security; it's about forging an<br />
                    entirely new foundation of trust in the realm of fintech.</p>
            </div>
            <div className='PointsContentTwo'>
                <p>
                    From contracts to transactions, every iota of data etches its legacy onto<br />
                    the indelible ledger of the blockchain. It's akin to a digital time capsule,<br />
                    immune to the eroding effects of time. Now, consider the concept of<br />
                    time-signing through blockchain-based e-signatures. It's more than just<br />
                    a signature; it's a stamp that echoes across time, forever validating the<br />
                    'when' of every agreement.
                </p>
                <img src='Ellipse 13semi circle.svg' />
            </div>
        </div>
    )
}

export default ServicesFrameTwo;