import './ServicesFrameFour.css';

const ServicesFrameFour = () => {
    return (
        <div className='ServicesFrameFourContainer'>
            <div>
                <h4>Decentralise. Simplify. Digitilise</h4>
                <p>
                    We’re a diverse and passionate team that takes<br />
                    ownership of your problems and empowers you to<br />
                    execute your plans. We stay light on our feet and truly<br />
                    enjoy delivering great work.
                </p>
                <div className='ServiceFooterMailFeild'>
                    <p>Get in Touch</p>
                    <div className="ServiceFooterContainer">
                        <div className="FooterContainer_Item">
                            <form className="FooterForm">
                                <input type="email" className="form__field" placeholder="Your E-Mail Address" />
                                <button className="btn btn--primary btn--inside uppercase">Send</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className='ServicesFooter'>
                    <img src='Ellipse 13blue.svg' />
                    <p>
                        <span>&lt;</span>Miyagi<span>&gt;</span><br />
                        A <span>decentralised</span> application built for the <span>curious</span>,<br />
                        By the a bunch of <span>dreamers</span> & <span>tinkers</span> on the <span>Silicon Platuea</span>, in the <span>Indian Subcontinent</span>.<br />
                        For The Crazy and The Curoius.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default ServicesFrameFour;