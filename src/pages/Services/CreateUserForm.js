import React, { useState } from 'react';

function DocumentUploadForm() {
    const [file, setFile] = useState(null);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [contractAddress, setContractAddress] = useState('');
    const [result, setResult] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        const formData = new FormData();
        formData.append('file', file);
        formData.append('name', name);
        formData.append('description', description);
        formData.append('contractAddress', contractAddress);
        console.log('upload', file, name);

        try {
            const response = await fetch('/upload', {
                method: 'POST',
                body: formData,
            });

            if (response.ok) {
                const data = await response.json();
                setResult(`Document Hash: ${data.documentHash}\nUnique Tag: ${data.uniqueTag}\nTransaction Hash: ${data.receipt.transactionHash}`);
            } else {
                const errorData = await response.text();
                setResult(`Error: ${errorData}`);
            }
        } catch (error) {
            setResult(`Error: ${error.message}`);
        }
    };

    return (
        <div className="DocumentUploadForm">
            <h2>Document Upload</h2>
            <form onSubmit={handleSubmit}>
                <label>
                    Select a file:
                    <input type="file" onChange={(e) => setFile(e.target.files[0])} />
                </label>
                <br />
                <label>
                    Name:
                    <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                </label>
                <br />
                <label>
                    Description:
                    <textarea value={description} onChange={(e) => setDescription(e.target.value)} />
                </label>
                <br />
                <label>
                    Contract Address:
                    <input type="text" value={contractAddress} onChange={(e) => setContractAddress(e.target.value)} />
                </label>
                <br />
                <button type="submit">Upload</button>
            </form>
            <div className="result">
                <pre>{result}</pre>
            </div>
        </div>
    );
}

export default DocumentUploadForm;
