import { Link } from 'react-router-dom';
import './ProjectsPage.css';

const ProjectsPage = () => {
    return (
        <div className="ProjectsPageMainContainer">
            <img className='circle23' src='../../../Ellipse 13blue.svg' />
            <div className='homebuttonlogo'>
                <Link to='/'>
                    &lt;<span style={{ color: 'black' }}>HOME</span>&gt;
                </Link>
            </div>
            <div className='ProjectsPagePartOne'>
                <div className='acntAddProjButtons'>
                    <button className='myAcntButton'><i className="fa-solid fa-user pe-3 fs-3"></i>My Account</button>
                    <button className='addProjButton'><i className="fa-solid fa-plus pe-3 fs-3"></i>Add Projects</button>
                </div>
                <div>
                    <h3 className='ps-4 pb-2 fs-2'>My Projects & Shared Projects</h3>
                    <div className='allProjectsBlock'>
                        <div>
                            <p><i className="fa-brands fa-hive pe-2" style={{ color: '#0069ec' }}></i>Creator</p>
                            <h5>MIYAGI</h5>
                            <Link to='/'>
                                <button>Open Project</button>
                            </Link>
                        </div>
                        <div>
                            <p><i className="fa-brands fa-hive pe-2" style={{ color: '#0069ec' }}></i>Contributer</p>
                            <h5>CYRUS</h5>
                            <button>Open Project</button>
                        </div>
                        <div>
                            <p><i className="fa-brands fa-hive pe-2" style={{ color: '#0069ec' }}></i>Observer</p>
                            <h5>Alexander</h5>
                            <button>Open Project</button>
                        </div>
                        <div>

                        </div>
                    </div>
                </div>
            </div>
            <div className='projectsPagePartTwo'>
            </div>
        </div>
    )
}

export default ProjectsPage;