import './PhilosophyFrameFour.css';

const PhilosophyFrameFour = () => {
    return (
        <div className='philosophyFrameFourContainer'>
            <div className='sectionOne'>
                <img src='Ellipse 13semi circle.svg' />
            </div>
            <div className='sectionTwo'>
                <img src='Ellipse 13blue.svg' />
                <h3>
                    If you believe in <span>Data ownership, Tech crasftmanship</span> and <span>The story of David and Goilath </span><br/>
                    then we would recommend you to try out <span>Miyagi</span>.<br/>
                    We won’t promise to be the best in all the segments that our product cover but we promise
                    that we would be the <span>least worst</span> because only Nature can be perfect and rest are just try
                    hards and <span>we wear that badge proudly</span>.
                </h3>
            </div>
        </div>
    )
}

export default PhilosophyFrameFour;