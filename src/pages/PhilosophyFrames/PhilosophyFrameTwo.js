import './PhilosophyFrameTwo.css';

const PhilosophyFrameTwo = () => {
    return (
        <div className="philosophyFrameTwoContainer">
            <h1>
                “Power to the People”
            </h1>
            <h3>
                &lt;Weather that be having the <span>Easy Verification Protocol</span> and
                <span> Timestamping the Documents</span> or to empower our users with
                the sheer simplicity of <span>Decentralised Data Storage</span>, we do it
                all and this is not just the end but the <span>tip of ice berg</span>&gt;
            </h3>
            <div className='d-flex justify-content-between'>
                <h5>
                    Just like <span>Satoshi</span> had envisoned that Bitcoin would take over the big
                    financial institution by dreaming of a world which is <span>fair</span> & <span>democratised</span>
                    in the financal industry, We at <span>Miyagi</span> want to create the same level of
                    disruption in the cloud storage ecosystem by leveraging the fundamental
                    of <span>Blockchain Technology</span> and core principles of <span>Satoshi’s philosophy</span>.
                </h5>
                <img src='Ellipse 13semi circle.svg' />
            </div>
        </div>
    )
}

export default PhilosophyFrameTwo;