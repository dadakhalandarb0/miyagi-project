import './PhilosophyFrameOne.css';

const PhilosophyFrameOne = () => {
    return (
        <div className="philosophyContainerOne">
            <img className='circleImage90deg' src="Ellipse 13blue.svg" />
            <img className='cubeImage' src='1_3cube.svg' />
            <div>
                <h1 className='fw-bold'>&lt;Our <span style={{ color: '#0069ec' }}>Philosophy</span>&gt;</h1>
                <h4>&lt;If we had to explain to a 5 year old what <span style={{ color: '#0069ec' }}>Miyagi</span> is,
                    We are basically a company which gives right to data
                    ownership to the client&gt;
                </h4>
                <h6>
                    <span>In an era</span> where people’s data is being exploited by big tech giants for their
                    ulterior motives we want to equip people with a tool which would <span>give you an </span>
                    <span>option to not let your data be exploited</span> by the big tech companies or perhaps
                    how <span>John Lennon</span> would’ve said it.
                </h6>
            </div>
        </div>
    )
}

export default PhilosophyFrameOne;