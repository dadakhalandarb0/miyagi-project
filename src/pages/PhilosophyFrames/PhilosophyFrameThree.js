import './PhilosophyFrameThree.css';

const PhilosophyFrameThree = () => {
    return (
        <div className='philosophyFrameThreeContainer'>
            <img src='Ellipse 13blue.svg' />
            <div>
                <h2>That’s a lot to make upto, and it does get stressful
                    for the team but <span>who doesn’t like a challenge?</span>
                </h2>
                <h5>
                    <span>&lt;Try out Miyagi for free and let us know what you feel about it</span>, your opinion matters the most to
                    us. Long time ago someone had told to one of our founders <span>“What does the user want?”</span> And that
                    still remains one of the core philosophies of <span>Miyagi</span>. We don’t want to act like the know it all type of
                    companies who say we’ve figured it all out but we want you to give us feedback and let us know
                    what you feel what we haven’t figured out and maybe <span>you’ll help one of our founders answer the </span>
                    <span>question</span> that was asked to him&gt;
                </h5>
            </div>
        </div>
    )
}

export default PhilosophyFrameThree;

