import { useState } from "react";
import './HeaderComponent.css';
import { Link } from 'react-router-dom';
import './Services.css';
import ServicesFrameOne from "./Services/ServicesFrameOne";
import ServicesFrameTwo from "./Services/ServicesFrameTwo";
import ServicesFrameThree from "./Services/ServicesFrameThree";
import ServicesFrameFour from "./Services/ServicesFrameFour";
const Services = () => {
    const [color, setColor] = useState(false);
    const [selectedWord, setSelectedWord] = useState('Education');

    const changeColor = () => {
        if (window.scrollY >= 100) {
            setColor(true);
        } else {
            setColor(false);
        }
    }
    window.addEventListener('scroll', changeColor);

    return (
        <div className="mainContainerServices">
            <div className={color ? 'header header-bg' : 'header'}>
                <Link to='/'>
                    <div className={color ? "logo fs-2 text-dark fw-bold" : "logo fs-2 text-dark fw-bold"}><span style={{ color: '#0069ec' }}>&lt;</span>MIYAGI<span style={{ color: '#0069ec' }}>&gt;</span></div>
                </Link>
                <nav>
                    <ul className={color ? "ulScroll mb-2 mt-2 p-0" : "mb-2 mt-2 p-0"}>
                        <li style={{ position: 'relative', top: '-4px' }}>
                            <a href="/" className="style-2 ">HOME</a>
                        </li>
                        <li style={{ position: 'relative', top: '-3px' }}>
                            <a href="/philosophy" className="style-2 ">PHILOSOPHY</a>
                        </li>
                        <li>
                        </li>
                        <li style={{ position: 'relative', top: '-3px' }}>
                            <a href="/sevices" className="style-2 ">SERVICES</a>
                        </li>
                        <li style={{ position: 'relative', top: '-4px' }}>
                            <a href="#" className="style-2 ">SUBSCRIPTION</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <ServicesFrameOne setSelectedWord={setSelectedWord} />
            <ServicesFrameTwo selectedWord={selectedWord} />
            <ServicesFrameThree />
            <ServicesFrameFour />
        </div>
    )
}

export default Services;