import { Link } from 'react-router-dom';
import './LoginPage.css';

const LoginPage = () => {
    return (
        <div className="loginPageMainContainer">
            <img className='cube00' src="1_3cube.svg" />
            <img className='cube11' src="1_3cube.svg" />
            <img className='cube22' src="1_3cube.svg" />
            <img className='circle22' src='Ellipse 13blue.svg' />
            <div className='homebuttonlogo'>
                <Link to='/'>
                    &lt;<span style={{ color: 'black' }}>Miyagi</span>&gt;
                </Link>
            </div>
            <div className='LoginButtonLogo'>
                <Link to='/signup'>
                    &lt;<span style={{ color: 'black' }}>SignUp</span>&gt;
                </Link>
            </div>
            <div className='partOne'>
                <h2>&lt;Login&gt;</h2>
                <h6>&lt;Welcome <span>Back</span>&gt;</h6>
                <div className="row">
                    <span>
                        <input className="basic-slide shadow" id="email" type="text" placeholder="Your favorite email" />
                        <label for="email">Email</label>
                    </span>
                </div>
                <Link to='/login/otp'>
                    <button className='continueButton'>
                        Continue
                    </button>
                </Link>
            </div>
            <div className='partTwo'>
                <a class="btnfos btnfos">
                    <img src='icons8-google-logo-48.png' className='me-2' style={{ width: '2rem', height: '2rem' }} />
                    Login with Google
                </a>
                <a class="btnfos btnfos">
                    <img src='icons8-apple-logo-50.png' className='me-2' style={{ width: '2rem', height: '2rem' }} />
                    Login with Apple
                </a>
            </div>
        </div>
    )
}

export default LoginPage;