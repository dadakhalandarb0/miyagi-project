import React, { useState } from "react";
import { Link } from 'react-router-dom';
import axios from "axios";
import './ProfileDetail.css';

const ProfileDetail = () => {
    const [userName, setUserName] = useState("");
    const [profilePictureFile, setProfilePictureFile] = useState(null);
    const [showNotification, setShowNotification] = useState(false);

    const handleImageChange = (event) => {
        const file = event.target.files[0];
        if (file) {
            setProfilePictureFile(file);
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!userName || !profilePictureFile) {
            setShowNotification(true); // Show the notification modal
            return;
        }

        try {
            const formData = new FormData();
            formData.append("userName", userName);
            formData.append("profilePicture", profilePictureFile);

            const response = await axios.post(
                "http://localhost:4000/createUser",
                formData,
                {
                    headers: {
                        "Content-Type": "multipart/form-data",
                    },
                }
            );

            console.log("User created:", response.data.message);
            // Redirect to the next page after successful user creation
            window.location.href = '/signup/profiledetail/subscription';
        } catch (error) {
            console.error("Error creating user:", error);
        }
    };

    return (
        <div className="profileDetailContainer">
            <img className='cube00' src="../1_3cube.svg" />
            <img className='cube11' src="../1_3cube.svg" />
            <img className='cube22' src="../1_3cube.svg" />
            <img className='circle22' src='../Ellipse 13blue.svg' />
            <div className='homebuttonlogo'>
                <Link to='/'>
                    &lt;<span style={{ color: 'black' }}>Miyagi</span>&gt;
                </Link>
            </div>
            <div className='profilePartOne'>
                <h4>&lt;
                    You will be <span>good to go</span> after this,<br />
                    We apologise keep you awaiting to use <span>Miyagi </span>&gt;
                </h4>
                {profilePictureFile && (
                    <div>
                        <img
                            src={URL.createObjectURL(profilePictureFile)}
                            alt="Selected"
                            className='rounded-circle'
                            style={{ maxWidth: "12rem", height: '12.2rem', position: 'absolute', zIndex: '1', right: '36.6rem' }}
                        />
                        <div className="p-image">
                            <label htmlFor="fileInput">
                                <i className="fa fa-camera upload-button"></i>
                            </label>
                            <input id="fileInput" className="file-upload" type="file" accept="image/*" onChange={handleImageChange} />
                        </div>
                    </div>

                )}
                <div className="imageWrapper">
                    <div className="image-upload">
                        <input
                            type="file"
                            accept="image/*"
                            onChange={handleImageChange}
                        />
                        <i className="fa fa-arrow-up"></i>
                    </div>
                </div>

                <h5 className='pt-4 m-0 fw-bold'>UPLOAD A PROFILE PICTURE</h5>
            </div>
            <div className='profilePartTwo'>
                <input
                    type='text'
                    placeholder='What would you like us to call you?'
                    className="btnfos1"
                    value={userName}
                    onChange={(e) => setUserName(e.target.value)}
                />
                <div className="select">
                    <select>
                        <option>Where did you hear about Miyagi?</option>
                        <option value="1">Search Engine (Google, Bing, etc.)</option>
                        <option value="2">Social Media</option>
                        <option value="3">Througn a Friend</option>
                        <option value="4">Ad</option>
                    </select>
                </div>
                <a href="/signup/profiledetail/subscription"
                    className="my-super-cool-btn"
                    onClick={handleSubmit}
                    data-bs-toggle="modal"
                    data-bs-target="#notificationModal"
                >
                    <div className="dots-container">
                        <div className="dot"></div>
                        <div className="dot"></div>
                        <div className="dot"></div>
                        <div className="dot"></div>
                    </div>
                    <span>Next!</span>
                </a>
            </div>
            <div className="modal fade" id="notificationModal" tabIndex="-1" aria-labelledby="notificationModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="notificationModalLabel">Notification</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={() => setShowNotification(false)}></button>
                        </div>
                        <div className="modal-body">
                            <p>Please fill in the username and select a profile picture.</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary bg-danger" data-bs-dismiss="modal" onClick={() => setShowNotification(false)}>OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProfileDetail;