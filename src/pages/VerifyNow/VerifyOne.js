import { useTypewriter, Cursor } from 'react-simple-typewriter';
import './VerifyOne.css';
import { useRef, useState } from 'react';
import { Link } from 'react-router-dom';

const VerifyOne = () => {

    const [text] = useTypewriter({
        words: ["Miyagi"],
        loop: {},
        typeSpeed: 200,
        deleteSpeed: 150,
    });

    const [isPdfModalOpen, setIsPdfModalOpen] = useState(false); // State for success modal visibility
    const [isFailureModalOpen, setIsFailureModalOpen] = useState(false); // State for failure modal visibility
    const fileInputRef = useRef(null); // Reference to the file input element

    const currentDateTimeRef = useRef(null); // Reference to store current date and time

    const handleFileSelect = () => {
        const selectedFile = fileInputRef.current.files[0];
        if (selectedFile) {
            const fileName = selectedFile.name;
            const fileExtension = fileName.split('.').pop().toLowerCase();

            if (fileExtension === 'pdf') {
                setIsPdfModalOpen(true); // Show the success modal for PDF
                setIsFailureModalOpen(false); // Close the failure modal

                // Capture the current date and time
                const now = new Date();
                const currentFormattedDateTime = now.toLocaleString('en-US', {
                    day: 'numeric',
                    year: 'numeric',
                    month: 'long',
                    hour: '2-digit',
                    minute: '2-digit',
                    second: '2-digit'
                });
                currentDateTimeRef.current = currentFormattedDateTime;
            } else {
                setIsPdfModalOpen(false); // Close the success modal
                setIsFailureModalOpen(true); // Show the failure modal for other file types
            }
        }
    };

    const handleClosePdfModal = () => {
        setIsPdfModalOpen(false); // Close the success modal
        fileInputRef.current.value = ""; // Clear the file input value
    };

    const handleCloseFailureModal = () => {
        setIsFailureModalOpen(false); // Close the failure modal
        fileInputRef.current.value = ""; // Clear the file input value
    };

    return (
        <div className="VerifyMain">
            <img className='cubeV' src="1_3cube.svg" />
            <img className='cube1V' src="1_3cube.svg" />
            <img className='cube2V' src="1_3cube.svg" />
            <p className="fs-1 text-uppercase"
                style={{
                    color: 'black',
                    fontFamily: 'outfit',
                    fontWeight: '1000',
                    paddingTop: '9.3rem'
                }}>
                &lt; Welcome To {" "}
                <span className="words" style={{ color: '#0069ec' }}>
                    {text}
                </span>
                <Cursor />
                &gt;
            </p>
            <div className='FileId'>miyagi file id</div>
            <img className='cube3V' src="Ellipse 13blue.svg"
                style={{ width: "15rem", height: '15rem' }}
            />
            <div className='w-100 h-100 p-3 m-0 text-center text-uppercase' style={{ backgroundColor: '#0069ec', marginTop: 'auto' }}>
                <input type="file" id='upload' ref={fileInputRef} hidden accept=".pdf, .jpg, .jpeg, .png" onChange={handleFileSelect} />
                <label className='DragAndDrop' htmlFor="upload">
                    <i className="fa-solid fa-file-import pe-3"></i>
                    Drag and drop
                </label>
            </div>

            {/* Success Modal for PDF */}
            {isPdfModalOpen && (
                <div className="custom-modal1">
                    <div className="modal-content1">
                        <button className="close-button" onClick={handleClosePdfModal}>&times;</button>
                        <h5 className="modal-title">This document was issued by Miyagi Labs LLP, Timestamped on,</h5>
                        <h5>{currentDateTimeRef.current}</h5>
                        <p>This Document was issued by a verified organisation.</p>
                        <div className='verify-buttons'>
                            <button onClick={handleClosePdfModal}>View on the chain</button>
                            <Link to='/verifynow/affiliation'>
                                <button>View the Issuing Body</button>
                            </Link>
                        </div>
                    </div>
                </div>
            )}

            {/* Failure Modal for Other File Types */}
            {isFailureModalOpen && (
                <div className="custom-modal1">
                    <div className="modal-content1">
                        <button className="close-button" onClick={handleCloseFailureModal}>&times;</button>
                        <h5 className="modal-title">This document was issued by Miyagi Labs LLP, Timestamped on,</h5>
                        <h5>{currentDateTimeRef.current}</h5>
                        <div className='verify-buttons'>
                            <button onClick={handleCloseFailureModal}>View on the chain</button>
                            <Link to='/verifynow/registerAffiliation'>
                                <button>View the Issuing Body</button>
                            </Link>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default VerifyOne;