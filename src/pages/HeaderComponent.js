import { useState } from "react";
import './HeaderComponent.css';
import { Link } from 'react-router-dom';
import LoginPage from "./LoginPage";

const Header = () => {
    const [color, setColor] = useState(false);
    const changeColor = () => {
        if (window.scrollY >= 100) {
            setColor(true);
        } else {
            setColor(false);
        }
    }
    window.addEventListener('scroll', changeColor);

    return (
        <>
            <div className={color ? 'header header-bg' : 'header'}>
                <Link to='/'>
                    <div className={color ? "logo fs-2 text-dark fw-bold" : "logo fs-2 text-dark fw-bold"}><span style={{ color: '#0069ec' }}>&lt;</span>MIYAGI<span style={{ color: '#0069ec' }}>&gt;</span></div>
                </Link>
                <nav>
                    <ul className={color ? "ulScroll mb-2 mt-2 p-0" : "mb-2 mt-2 p-0"}>
                        <li style={{ position: 'relative', top: '-4px' }}>
                            <a href="/" className="style-2 ">HOME</a>
                        </li>
                        <li style={{ position: 'relative', top: '-3px' }}>
                            <a href="/philosophy" className="style-2 ">PHILOSOPHY</a>
                        </li>
                        <li>
                        </li>
                        <li style={{ position: 'relative', top: '-3px' }}>
                            <a href="/services" className="style-2">SERVICES</a>
                        </li>
                        <li style={{ position: 'relative', top: '-4px' }}>
                            <a href="/signup/profiledetail/subscription" className="style-2">SUBSCRIPTION</a>
                        </li>
                    </ul>
                </nav>
                <div class="buttons">
                    <button><Link to='/waitlist' style={{ color: '#004B93' }}>SignUp</Link></button>
                    <button><Link to='/waitlist' style={{ color: '#CF2850' }}>Login</Link></button>
                </div>
            </div>
        </>
    )

}

export default Header;