import React, { useState, useEffect, useRef } from "react";
import Croppie from "croppie";
import "../assets/CropImageUploader.css"; // Import your CSS file

const CropImageUploader = () => {
    const [selectedImage, setSelectedImage] = useState(null);
    const croppieRef = useRef(null);
    const [isModalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        if (selectedImage && croppieRef.current) {
            const reader = new FileReader();
            reader.onload = function (e) {
                const rawImg = e.target.result;

                croppieRef.current = new Croppie(document.getElementById("upload-demo"), {
                    viewport: {
                        width: 160,
                        height: 160,
                        type: "circle",
                    },
                    enableExif: true,
                });

                croppieRef.current.bind({
                    url: rawImg,
                }).then(() => {
                    console.log("Croppie bind complete");
                });
            };
            reader.readAsDataURL(selectedImage);
        }
    }, [selectedImage]);

    const handleImageChange = (e) => {
        const file = e.target.files[0];
        if (file) {
            setSelectedImage(file);
            openCropModal();
        }
    };

    const handleCropImage = () => {
        if (selectedImage && croppieRef.current) {
            croppieRef.current.result({
                type: "base64",
                format: "png",
                size: { width: 160, height: 160 },
            }).then((resp) => {
                console.log("Cropped image:", resp);
                closeCropModal();
            });
        }
    };

    const openCropModal = () => {
        const modal = document.getElementById("cropImagePop");
        modal.classList.add("show");
        modal.style.display = "block";
    };

    const closeCropModal = () => {
        const modal = document.getElementById("cropImagePop");
        modal.classList.remove("show");
        modal.style.display = "none";
    };

    return (
        <div className="container text-center mb-5 mt-5">

            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <div className="confirm-identity">
                                <div className="ci-user d-flex align-items-center justify-content-center">
                                    <div className="ci-user-picture">
                                        <img
                                            src=""
                                            id="item-img-output"
                                            className="imgpreviewPrf img-fluid"
                                            alt=""
                                        />
                                    </div>
                                </div>
                                <div className="ci-user-btn text-center mt-4">
                                    <label className="userEditeBtn btn-default bg-blue position-relative">
                                        Update Profile Photo
                                        <input
                                            type="file"
                                            className="item-img file center-block filepreviewprofile"
                                            onChange={handleImageChange}
                                        />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className={`modal fade cropImageModal ${isModalVisible ? "show" : ""}`} id="cropImagePop" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <button type="button" className="close-modal-custom" onClick={closeCropModal} aria-label="Close">
                    <i className="feather icon-x"></i>
                </button>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-body p-0">
                            <div className="modal-header-bg"></div>
                            <div className="up-photo-title">
                                <h3 className="modal-title">Update Profile Photo</h3>
                            </div>
                            <div className="up-photo-content pb-5">
                                <div id="upload-demo" className="center-block">
                                    <h5><i className="fas fa-arrows-alt mr-1"></i> Drag your photo as you require</h5>
                                </div>
                                <div className="upload-action-btn text-center px-2">
                                    <button type="button" id="cropImageBtn" className="btn btn-default btn-medium bg-blue px-3 mr-2" onClick={handleCropImage}>
                                        Save Photo
                                    </button>
                                    <button type="button" className="btn btn-default btn-medium bg-default-light px-3 ml-sm-2 replacePhoto position-relative">
                                        Replace Photo
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CropImageUploader;
