import { initializeApp } from "firebase/app"
import { getAnalytics } from "firebase/analytics"

const firebaseConfig = {
  apiKey: "AIzaSyA4ZYAMS2aGzL3dflp1PlY7r0WTvNwxxho",
  authDomain: "miyagi-fb.firebaseapp.com",
  projectId: "miyagi-fb",
  storageBucket: "miyagi-fb.appspot.com",
  messagingSenderId: "684638535080",
  appId: "1:684638535080:web:480c52104e599d932cb19d",
  measurementId: "G-1GMJ86RNV9",
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const analytics = getAnalytics(app)
