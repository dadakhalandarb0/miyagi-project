// Import required libraries and modules
const Web3 = require("web3")
const express = require("express")
const { create } = require("ipfs-http-client")
const multer = require("multer")
const { PDFDocument } = require("pdf-lib")
const fs = require("fs")
const Hash = require("ipfs-only-hash")
const middleware = require("./middleware/index") //for using the authentication in here.
const cors = require("cors")

const abi = require("../artifacts/contracts/DocuUpload.sol/DocuUpload.json").abi

require("dotenv").config()

const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())

// Set up Web3 connection and account
const web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545"))
const privateKey = process.env.PRIVATE_KEY
const account = web3.eth.accounts.privateKeyToAccount(privateKey)
web3.eth.accounts.wallet.add(account)
web3.eth.defaultAccount = account.address

// Contract details
const contractAddress = "0x5FbDB2315678afecb367f032d93F642f64180aa3"
const DocuUpload = new web3.eth.Contract(abi, contractAddress)

const uploadingFunctions = require("./functions/uploading")

// Set up IPFS
const projectId = process.env.INFURA_KEY
const projectSecret = process.env.INFURA_SECRET

const auth =
  "Basic " + Buffer.from(projectId + ":" + projectSecret).toString("base64")

const ipfs = create({
  host: "infura-ipfs.io",
  port: 5001,
  protocol: "https",
  headers: {
    authorization: auth,
  },
})

const upload = multer({ storage: multer.memoryStorage() })

// API endpoint for uploading a document
app.post(
  "/upload",
  middleware.decodeToken(),
  upload.single("file"),
  async (req, res) => {
    try {
      // Parse request data
      const file = req.file
      const { name, description, contractAddress } = req.body

      // Validate required fields
      if (!file || !name || !description || !contractAddress) {
        return res.status(400).send("Missing required fields")
      }

      // Calculate file size in MB and check against allowed storage
      const fileSizeMB = file.size / (1024 * 1024)
      const allowedMB = await DocuUpload.methods
        .getAllowedGB(account.address)
        .call()
      console.log("Size of file and allowedGB is: ", fileSizeMB, allowedMB)
      if (fileSizeMB > allowedMB) {
        return res
          .status(400)
          .send("File size exceeds allowed storage capacity")
      }

      // Upload file to IPFS
      const ipfsResult = await ipfs.add(file.buffer)
      const documentHash = ipfsResult.path

      // Estimate gas and send transaction to upload the document to the blockchain
      const gasEstimate = await DocuUpload.methods
        .uploadDocument(
          account.address,
          name,
          description,
          documentHash,
          fileSizeMB
        )
        .estimateGas({ from: account.address })

      // Generate a unique tag for the document
      const uniqueTag = await uploadingFunctions.generateUniqueTag(
        account.address
      )

      // Send transaction and respond with relevant data
      const receipt = await DocuUpload.methods
        .uploadDocument(
          account.address,
          name,
          description,
          documentHash,
          fileSizeMB
        )
        .send({ from: account.address, gas: gasEstimate })

      res.send({ receipt, documentHash, uniqueTag })
    } catch (err) {
      res.status(500).send(err.toString())
    }
  }
)

// API endpoint to increment allowed storage capacity for a user
app.post("/incrementAllowedMB", middleware.decodeToken(), async (req, res) => {
  const { additionalMB } = req.body
  let receipt

  if (!additionalMB) {
    return res.status(400).send("Missing required fields")
  }
  console.log("Incrementing ::", req.user.uid)

  const userID = req.user.uid

  try {
    const gasEstimate = await DocuUpload.methods
      .incrementAllowedUploads(userID, additionalMB)
      .estimateGas({ from: account.address })

    receipt = await DocuUpload.methods
      .incrementAllowedUploads(userID, additionalMB)
      .send({ from: account.address, gas: gasEstimate })
  } catch (error) {
    console.log("Error occured!" + error)
    res.status(400).json({
      success: false,
      message: "Failed, check whether MB is there." + error,
    })
    return
  }

  res.send({ receipt })
})

// API endpoint to delete a document
app.post("/deleteDocument", middleware.decodeToken(), async (req, res) => {
  const { index } = req.body
  const userID = req.user.uid

  if (!userID || !index) {
    return res.status(400).send("Missing required fields")
  }
  // Estimate gas and send transaction to delete the document
  try {
    const gasEstimate = await DocuUpload.methods
      .deleteDocument(userID, index)
      .estimateGas({ from: account.address })

    const receipt = await DocuUpload.methods
      .deleteDocument(userID, index)
      .send({ from: account.address, gas: gasEstimate })
  } catch (error) {
    console.log("Error occured!" + error)
    res.status(400).json({
      success: false,
      message: error,
    })
    return
  }
  res.send({ receipt })
})

//BELOW IS CALLED AFTER THE USER ENTERS THEIR USER NAME.
app.post("/createUser", middleware.decodeToken(), async (req, res) => {
  const userId = req.user.uid
  const { userName, profilePicturedataURI } = req.body

  let userCreate
  if (!userName) {
    return res.status(400).send("Missing required fields")
  }
  try {
    userCreate = await DocuUpload.methods
      .createUser(userId, userName, profilePicturedataURI, 1000)
      .send({ from: account.address })
  } catch (error) {
    console.log("Error occured!" + error)
    res.status(400).json({
      success: false,
      message: error,
    })
    return
  }
  res.send({ userCreate })
})

// API endpoint to get all documents of a user
app.post("/getAllDocuments", middleware.decodeToken(), async (req, res) => {
  const userId = req.user.uid
  let documents
  if (!userId) {
    return res.status(400).send("Missing required fields")
  }
  try {
    documents = await DocuUpload.methods.getAllDocuments(userId).call()
  } catch (error) {
    console.log("Error occured!" + error)
    res.status(400).json({
      success: false,
      message: error,
    })
    return
  }
  res.send({ documents })
})

// API endpoint to get the count of uploaded documents for a user
app.post(
  "/getUploadedDocumentCount",
  middleware.decodeToken(),
  async (req, res) => {
    const userId = req.user.uid
    let count
    if (!userId) {
      return res.status(400).send("Missing required fields")
    }
    try {
      count = await DocuUpload.methods.getUploadedDocumentCount(userId).call()
    } catch (error) {
      console.log("Error occured!" + error)
      res.status(400).json({
        success: false,
        message: error,
      })
      return
    }
    res.send({ count })
  }
)

// API endpoint to get the allowed storage capacity for a user
app.post("/getAllowedMB", middleware.decodeToken(), async (req, res) => {
  const userId = req.user.uid
  let allowedMB
  if (!userId) {
    return res.status(400).send("Missing required fields")
  }

  try {
    allowedMB = await DocuUpload.methods.getAllowedMB(userId).call()
  } catch (error) {
    console.log("Error occured!" + error)
    res.status(400).json({
      success: false,
      message: error,
    })
    return
  }
  res.send({ allowedMB })
})

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//For PDF uplaoading

app.post(
  "/uploadPDF",
  middleware.decodeToken(),
  upload.single("file"),
  async (req, res) => {
    const userId = req.user.uid
    let fileName
    try {
      const file = req.file
      const pdfBytes = file.buffer
      fileName = req.file.filename
      console.log("File being uploaded...")

      const tag = req.body.tag
      const modifiedPdfBytes = await verificationFunctions.storeTagAsMetadata(
        pdfBytes,
        tag
      )

      // Upload the Modified PDF's to IPFS
      // const ipfsObj = await ipfs.add(modifiedPdfBytes)
      // const cid = ipfsObj.path

      const cid = await Hash.of(modifiedPdfBytes)

      // Store the tag in the smart contract

      await verificationFunctions.storeTagInSmartContract(tag, cid)
    } catch (error) {
      console.log("Error occured!" + error)
      res.status(400).json({
        success: false,
        message: "Failed, tag may already exist. " + error,
      })
      return
    }
    const modifiedPdfBuffer = Buffer.from(modifiedPdfBytes)
    const base64Pdf = modifiedPdfBuffer.toString("base64")
    res.status(200).json({ success: true, cid, pdfBytes: base64Pdf, fileName })
  }
)

app.post("/verifyPDF", upload.single("file"), async (req, res) => {
  const file = req.file
  const pdfBytes = file.buffer
  try {
    const accounts = await web3.eth.getAccounts()
    const fromAccount = accounts[0]
    // Fetch metadata from PDF
    const tag = await verificationFunctions.extractTagFromMetadata(pdfBytes)
    console.log(tag)
    if (tag == 0) {
      res.status(400).json({
        success: false,
        message: "Not verified, Tag not present.",
      })
      return
    }
    console.log("The tag is: ", tag)

    // Check if the CID is in the smart contract's mapping

    const storedCID = await contract.methods
      .getPDF(tag)
      .call({ from: fromAccount })

    // const ipfsObj = await ipfs.add(pdfBytes)
    const cid = await Hash.of(pdfBytes)
    // const cid = ipfsObj.path
    console.log(
      "The stored CID on the blockchain for this tag and current is: ",
      storedCID,
      " ",
      cid,
      " "
    )

    if (cid === storedCID) {
      res.status(200).json({ success: true })
    } else {
      res.status(400).json({ success: false, message: "Not verified." })
    }
  } catch (error) {
    res.status(400).json({ success: false, message: "Failed: " + error })
  }
})

app.post("/verifyTagPDF", async (req, res) => {
  const tag = req.body.text
  try {
    const accounts = await web3.eth.getAccounts()
    const fromAccount = accounts[0]

    console.log("Received Tag: ", tag)

    const storedCID = await contract.methods
      .getPDF(tag)
      .call({ from: fromAccount })
    console.log("StoredCID: ", storedCID)

    if (storedCID) {
      res
        .status(200)
        .json({ success: true, hash: storedCID, message: "Found." })
    } else {
      res.status(404).json({ success: false, message: "Not found." })
    }
  } catch (error) {
    res.status(400).json({ success: false, message: "Failed: " + error })
  }
})

// Start the server
app.listen(4000, () => console.log("Server running on port 4000"))
