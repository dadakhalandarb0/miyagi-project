async function storeTagAsMetadata(pdfBytes, tag) {
  const pdfDoc = await PDFDocument.load(pdfBytes, { updateMetadata: false })
  const existingKeywords = pdfDoc.getKeywords()
  let newKeywords
  if (existingKeywords) newKeywords = [tag, ...existingKeywords]
  else newKeywords = [tag]
  console.log("Keywords:", newKeywords)
  pdfDoc.setKeywords(newKeywords)
  const modifiedPdfBytes = await pdfDoc.save()
  return modifiedPdfBytes
}

async function extractTagFromMetadata(pdfBytes) {
  const pdfDoc = await PDFDocument.load(pdfBytes)
  const keywords = pdfDoc.getKeywords()
  if (typeof keywords == "undefined") {
    return [0, 0]
  } else {
    return keywords
  }
}

async function storeTagInSmartContract(tag, cid) {
  const accounts = await web3.eth.getAccounts()
  const fromAccount = accounts[0]
  console.log(tag, cid)
  await contract.methods.addPDF(tag, cid).send({ from: fromAccount })
  console.log("Tag addded to blockchain successfully.")
}

module.exports = {
  storeTagAsMetadata,
  extractTagFromMetadata,
  storeTagInSmartContract,
}
