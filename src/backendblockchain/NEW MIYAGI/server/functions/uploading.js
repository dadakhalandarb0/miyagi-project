async function generateUniqueTag(user) {
  const ranNumber = Math.floor(Math.random() * 1000000)
  const usrnm = user.split("").sort().join("")
  do {
    uniqueTag = `${usrnm}-${ranNumber}`
  } while (await DocuUpload.methods.checkTagExists(uniqueTag))
  return uniqueTag
}


module.exports = {generateUniqueTag}