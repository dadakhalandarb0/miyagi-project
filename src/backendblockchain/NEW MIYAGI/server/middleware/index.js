const admin = require("../config/firebase-config")

class Middleware {
  decodeToken() {
    return async (req, res, next) => {
      const token = req.headers.authorization.split(" ")[1]
      try {
        const decodeValue = await admin.auth().verifyIdToken(token)
        if (decodeValue) {
          console.log(decodeValue)
          req.user = decodeValue // Attach decoded value to req.user
          return next()
        }
        return res.json({ message: "Unauthorized" })
      } catch (e) {
        return res.json({ message: "Internal Error" })
      }
    }
  }
}

module.exports = new Middleware()
