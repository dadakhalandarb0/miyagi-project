pragma solidity ^0.8.0;

contract DocuUpload {
    //For uploading documents.
    struct Document {
        string docOwner;
        string name;
        string hash;
        uint256 timestamp;
        bool publicDoc;
        string tag;
        uint256 index;
    }

    struct User {
        string userName;
        string profilePicturedataURI;
        uint256 allowedMB;
        uint256 documentCount;
        uint256 documentCountLocked;
        mapping(uint256 => Document) documents;
    }
    mapping(string => Document) tagToDocument;
    mapping(string => User) users;
    address public owner;

    constructor() {
        owner = msg.sender;
    }

    //After the uploading of profile picture and setting of username do this.
    function createUser(
        string calldata user,
        string calldata userName,
        string calldata profilePicturedataURI,
        uint256 allowedMB
    ) external {
        require(msg.sender == owner, "Not authorized.");
        require(bytes(userName).length > 0, "Username cannot be empty.");
        require(allowedMB > 0, "Allowed MB must be greater than 0.");
        users[user].userName = userName;
        users[user].profilePicturedataURI = profilePicturedataURI;
        users[user].allowedMB = allowedMB;
    }

    function incrementAllowedUploads(
        string calldata user,
        uint256 additionalUploads
    ) external {
        require(msg.sender == owner, "Not authorized.");
        users[user].allowedMB += additionalUploads;
    }

    function uploadDocument(
        string calldata user,
        string calldata tag,
        string calldata name,
        string calldata ipfsHash,
        uint256 fileSizeMB,
        bool publicDoc
    ) external {
        require(msg.sender == owner, "Not authorized.");
        require(bytes(name).length > 0, "Name cannot be empty.");
        require(bytes(ipfsHash).length > 0, "IPFS hash cannot be empty.");
        require(fileSizeMB > 0, "File size must be greater than 0 MB.");

        // Store the file metadata on-chain
        uint256 index = users[user].documentCountLocked;
        users[user].documents[index] = Document(
            user,
            name,
            ipfsHash,
            block.timestamp,
            publicDoc,
            tag,
            index
        );
        tagToDocument[tag] = users[user].documents[index];
        users[user].documentCount += 1;
        users[user].documentCountLocked += 1;
        users[user].allowedMB -= fileSizeMB;
    }

    function deleteDocument(
        string calldata user,
        string calldata tag
    ) external {
        require(msg.sender == owner, "Not authorized.");
        require(users[user].documentCount > 0, "No documents to delete.");

        Document storage document = tagToDocument[tag];

        require(bytes(document.hash).length > 0, "Document not found.");
        require(
            keccak256(abi.encodePacked((user))) ==
                keccak256(abi.encodePacked((document.docOwner))),
            "Not your document."
        );

        delete users[user].documents[document.index];
        delete tagToDocument[tag];
        users[user].documentCount -= 1;
    }

    function getAllDocuments(
        string calldata user
    ) external view returns (Document[] memory) {
        Document[] memory docs = new Document[](users[user].documentCount);
        for (uint256 i = 0; i < users[user].documentCount; i++) {
            docs[i] = users[user].documents[i];
        }
        return docs;
    }

    function checkTagExists(string calldata tag) external view returns (bool) {
        if (bytes(tagToDocument[tag].hash).length > 0) {
            return true;
        } else {
            return false;
        }
    }

    function getUploadedDocumentCount(
        string calldata user
    ) external view returns (uint256) {
        require(msg.sender == owner, "Not authorized.");

        return users[user].documentCount;
    }

    function getAllowedMB(
        string calldata user
    ) external view returns (uint256) {
        require(msg.sender == owner, "Not authorized.");
        return users[user].allowedMB;
    }

    function getOwnDoc(
        string calldata user,
        string calldata tag
    ) external view returns (Document memory) {
        require(msg.sender == owner, "Not authorized");
        Document memory document = tagToDocument[tag];
        require(bytes(document.hash).length > 0, "Document not found.");
        require(
            keccak256(abi.encodePacked((user))) ==
                keccak256(abi.encodePacked((document.docOwner))),
            "Not your document."
        );
        return document;
    }

    function getOtherDoc(
        string calldata tag
    ) external view returns (Document memory) {
        require(msg.sender == owner, "Not authorized");
        Document memory document = tagToDocument[tag];
        require(bytes(document.hash).length > 0, "Document not found.");
        require(document.publicDoc, "This is a private document.");
        return document;
    }

    //For verification of PDFs
    struct PDF {
        string tag;
        string pdfHash;
    }

    mapping(string => PDF) private pdfs;

    event PDFAdded(string tag, string pdfHash);

    function addPDF(string memory tag, string memory pdfHash) public {
        require(bytes(pdfs[tag].pdfHash).length == 0, "Tag already exists");
        pdfs[tag] = PDF(tag, pdfHash);
        emit PDFAdded(tag, pdfHash);
    }

    function getPDF(string memory tag) public view returns (string memory) {
        require(bytes(pdfs[tag].pdfHash).length != 0, "Tag does not exist");
        return pdfs[tag].pdfHash;
    }
}
