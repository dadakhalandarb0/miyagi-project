import styles from "./ClipPathGroup.module.css";
const ClipPathGroup = ({ clipPathGroup }) => {
  return <img className={styles.clipPathGroup} alt="" src={clipPathGroup} />;
};

export default ClipPathGroup;
